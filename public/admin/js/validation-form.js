$(document).ready(function() {
	$('#add-product').validate({
		rules: {
			txt_proName: {
				required: true
			},
			txt_proPriceEntry: {
				required: true
			},
			txt_proPrice: {
				required: true
			},
			txt_proSale: {
				required: true
			},
			txt_proPriceSale: {
				required: true
			},
			txt_proDescription: {
				required: true
			},
			txt_cateId: {
				required: true
			}
		},
		messages: {
			txt_proName: {
				required: 'Please enter a product name'
			},
			txt_proPriceEntry: {
				required: 'Please enter a entry price'
			},
			txt_proPrice: {
				required: 'Please enter a product price'
			},
			txt_proSale: {
				required: 'Please enter a product sale'
			},
			txt_proPriceSale: {
				required: 'Please enter a sale price'
			},
			'txt_proDescription': {
				required: 'Please enter a product description'
			},
			'txt_cateId': {
				required: 'Please enter a product category'
			}
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	});

	$(document).on('click', '.close-image', function(e) {
		let id = $(this).attr('rel');
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  if (result.value) {
			 window.location.href = "/shop-admin/product/delete/media/"+ id;
		  }
		});
	});
});