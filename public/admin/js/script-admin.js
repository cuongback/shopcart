$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    },
});

$(document).ready(function() {
	//Initialize Select2 Elements
    $('.select2').select2();

    $("select[name='c_parentId'], select[name='txt_cateId']").select2({
      templateResult: function (data, container) {
        if (data.element) {
          $(container).addClass($(data.element).attr("class"));
        }
        return data.text;
      }
    });

    $( "#sortable" ).sortable();
    $('#description').summernote();

    tinymce.init({
        selector:'textarea.content',
        width: '100%',
        height: 600
    });
});

$(document).ready(function() {
    /*----------------------------------------*/
    /*      Attribute Value
    /*----------------------------------------*/
    $('.btn-value').click(function(e) {
        e.preventDefault();
        let n = Number($('.table-attribute tbody tr:last').find('input').attr('name').slice(13, -5)) + 1 || 0;

        $('.table-attribute tbody').append(
          '<tr><td class="drag-attribute"><i class="fas fa-bars"></i></td><td class="px-2"><input type="hidden" name="txtAttribute['+ n +'][id]" value=""><input type="text" required name="txtAttribute['+ n +'][value]" autocomplete="off" class="form-control"></td><td class="trash-attribute"><a href="javascript:void(0)" class="btn btn-default btn-remove" title="Delete Value"><i class="fa fa-trash"></i></a></td></tr>'
        );
    });    

    $('.table-attribute tbody').on('click', '.btn-remove', function(e) {
        e.preventDefault();
        $(this).closest('tr').remove();
    });    
});

$(function() {

    /*----------------------------------------*/
    /*      selectize Product
    /*----------------------------------------*/

    function selectize() {
        let selects = $('select.selectize');

        for (let select of selects) {
            $(select).selectize({
                delimiter: ',',
                persist:true,
                selectOnTab: true,
                valueField: 'id',
                labelField: 'name',
                searchField: 'name',
                hideSelected: false,
                allowEmptyOption: true,
                plugins: ['remove_button'],
            });
        }
    }

    function addAttributeValues(element, clearSelected = true) {
        let values = $(element).find('option:selected').data('values');
        
        let attributeValues = $('select[id="attribute.' + element.dataset.attributeId + '.values"]')[0].selectize;
        
        if(clearSelected) {
            attributeValues.clear();
        }
        
        attributeValues.clearOptions();

        let options = attributeValues.options;

        for(let id in values) {
            attributeValues.addOption({ id, name: values[id] });

            for (let i in options) {
                attributeValues.addItem(options[i].value);
            }
        }
    }

    selectize();

    $('.btn-add-value').on('click', function(e) {
        e.preventDefault();
        let n = Number($('.table-attribute tbody tr:last').find('select.attribute-group').data('attributeId')) + 1;
        let option = $('.table-attribute tbody tr:last').find('select.attribute-group').html().replace('selected=""', '');
        let html = '<tr>' +
                   '<td class="drag-attribute"><i class="fas fa-bars"></i></td>' +
                   '<td class="px-2" style="width: 200px;">' +
                   '<select class="form-control attribute-group" name="attributes['+ n +'][attribute_id]" data-attribute-id="' + n + '" required>' + option +
                   '</select></td><td class="px-2"><select class="selectize" name="attributes['+ n +'][values][]" id="attribute.' + n + '.values" required multiple></select></td><td class="trash-attribute"><button class="btn btn-default btn-remove" title="Delete Value"><i class="fa fa-trash"></i></button></td></tr>';
        $('.table-attribute tbody').append(html);
        selectize();
    });

    $('.product-attribute-wrapper').on('change', '.attribute-group', function(e) {
        addAttributeValues(e.currentTarget);
    });

    $('.attribute-group').has('option:selected').each(function(i, el) {
        addAttributeValues(el, false);
    });

    /*--------------------------------------------------------------------------------*/
    
    $(document).on('change', '.img-file', function() {
        var uploadFile = $(this);
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            reader.onloadend = function(){ // set image data as background of div
                //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                $('.image-preview').find('.fa-image').remove();
                $('.image-preview').find('.image-preview-border').append('<img src="'+ this.result + '" name="image" id="image" alt="image featured">');
            }
        }
    });    
});