require('./bootstrap');
import $ from 'jquery';

$(function () {	
	$('.list-promotion-info').owlCarousel({
	    loop:true,
	    nav:true,
	    lazyLoad:true,
	    navText: ["<span class='prev-slide'></span>","<span class='next-slide'></span>"],
	    responsive:{
	        0:{
	            items:2
	        },
	        768:{
	            items:3
	        },
	        992:{
	            items:4
	        }
	    }
	});

	$('.product-item-list').owlCarousel({
	    loop:true,
	    nav:true,
	    navText: ["<span class='prev-slide'></span>","<span class='next-slide'></span>"],
	    responsive:{
	        0:{
	            items:2
	        },
	        768:{
	            items:3
	        },
	        992:{
	            items:4
	        }
	    }
	});

	$('.product-item-related').owlCarousel({
	    loop:true,
	    nav:true,
	    navText: ["<span class='prev-slide'></span>","<span class='next-slide'></span>"],
	    responsive:{
	        0:{
	            items:2
	        },
	        768:{
	            items:3
	        }
	    }
	});

	$('.thumnail-image').slick({
		slidesToShow: 4,
        slideToScroll: 1,
        dots: false,
        arrows: true,
        infinite: false,
        centerMode: false,
        focusOnSelect: true,
        rows: 0,
        rtl: false,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 4,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                },
            },
        ]
	});

	// click to change image
	$('.img-gallery').on('click', function() {
		let imgSource = $(this).find('.img-thum').attr('val');
		
		$(this).closest('.slick-list').find('.card').removeClass('active-img');
		$(this).find('.card').addClass('active-img');
		$('.base-image .img-index').attr('src', imgSource);
	});

	// sidebar menu with overlay
	$('.mega-menu-overlay').on('click', function() {
		$('.top-category').removeClass('open');
		$('.mega-menu-overlay').removeClass('active');
	});

	$('.menu-cate').on('click', function() {
		$('.top-category').addClass('open');
		$('.mega-menu-overlay').addClass('active');

		$('i[aria-expanded=true]').attr('aria-expanded', 'false');
	});

	// hover category on home
	$('.cate_hover_home').hover(function() {
		$(this).closest('.product-category').find('.cate_hover_home').removeClass('active-cate');
		$(this).addClass('active-cate');

		$('.' + $(this).attr('idy')).hide();
		$('.' + $(this).attr('idx')).show();
	});
});
