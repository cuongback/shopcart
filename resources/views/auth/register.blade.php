@extends('frontend.layouts.master')

@section('title', 'Register Page')

@section('content')
    <div class="category-other-page">
        <div class="container">
            <div class="row no-gutters">
                @include('frontend.components.menu')
            </div>
        </div>
    </div>
    <div class="user-register">
    <div class="breadcrumb-info">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="#">Đăng ký tài khoản</a></li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <h1 class="user-login-title text-uppercase">Đăng ký tài khoản</h1>
        <div class="row">
            <div class="col-lg-12">
                <div class="user-login-form">
                    <form method="POST" action="">
                        <span class="mb-2 d-inline-block">Nếu chưa có tài khoản vui lòng đăng ký tại đây.</span>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="firstname">Họ <span class="user-required">*</span></label>
                                    <input type="text" id="firstname" class="form-control" name="firstname" placeholder="Họ">
                                </div>                              
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="lastname">Tên <span class="user-required">*</span></label>
                                    <input type="text" id="lastname" class="form-control" name="lastname" placeholder="Tên">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="email">Email <span class="user-required">*</span></label>
                                    <input type="email" id="email" placeholder="Email" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="password">Mật khẩu <span class="user-required">*</span></label>
                                    <input type="password" id="password" placeholder="Mật khẩu" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="phone">Điện thoại <span class="user-required">*</span></label>
                                    <input type="number" id="phone" placeholder="Điện thoại" class="form-control" required>
                                </div>
                            </div>
                        </div>
                                                
                        <div class="user-btn-form">
                            <input type="submit" class="btn user-btn btn-register" value="Đăng ký">
                            <a href="#" class="user-btn-login">Đăng nhập</a>
                        </div>
                    </form>
                </div>          
            </div>          
        </div>
        <div class="row">
            <div class="col-12">
                <div class="user-login-oauth">
                    <span>Hoặc đăng nhập bằng</span>
                    <div class="user-login-oauth-btn">
                        <a href="#" class="fb-oauth-btn">
                            <img src="{{ asset('images/fb-btn.svg') }}" width="129px" height="37px">
                        </a>
                        <a href="#" class="gp-oauth-btn">
                            <img src="{{ asset('images/gp-btn.svg') }}" width="129px" height="37px">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection