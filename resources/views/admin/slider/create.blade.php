@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Slider</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Slider</a></li>
            <li class="breadcrumb-item active">New</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
	<!-- Default card -->
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
		            <div class="card-header">
		              <h3 class="card-title">Add Slider</h3>
		            </div>
	            	<!-- /.card-header -->
		            
	            	<form method="POST" action="" class="form-horizontal" enctype="multipart/form-data">
	            		@csrf
	            		<div class="card-body">
		            		<div class="form-group">
			            		<div class="row">
			            			<label class="col-md-3">Slider Image <span class="text-danger">(*)</span></label>
			            			<div class="col-md-6">
			            				<div class="image-featured">
				                            <div class="image-preview">
				                                <div class="image-preview-border">
				                                  <i class="far fa-image" aria-hidden="true"></i>
				                                </div>
				                                <label class="btn btn-primary w-100" style="margin: 0;border-radius: 0;">Upload<input type="file" name="txt_image" class="img-file" style="
				                                    width: 0;
				                                    height: 0;
				                                    overflow: hidden;
				                                "></label>
				                                @if ($errors->first('txt_image'))
					            					<p class="text-danger">{{ $errors->first('txt_image') }}</p>
					            				@endif
				                            </div>
				                        </div>
			            			</div>
			            		</div>
			            	</div>
			            	<div class="form-group">
			            		<div class="row">
			            			<label for="title" class="col-md-3">Title</label>
			            			<div class="col-md-6">
			            				<input type="text" class="form-control" name="txt_title">
			            			</div>
			            		</div>			                
				            </div>
				            <div class="form-group">
				            	<div class="row">
				            		<label class="col-md-3">Link <span class="text-danger">(*)</span></label>
				            		<div class="col-md-6">
				            			<input type="text" class="form-control" name="txt_link" value="#">
				            			@if ($errors->first('txt_link'))
			            					<p class="text-danger">{{ $errors->first('txt_link') }}</p>
			            				@endif
				            		</div>
				            	</div>
				            </div>
				            <div class="form-group">
				            	<div class="row">
				            		<label class="col-md-3">Order</label>
				            		<div class="col-md-6">
					            		<input type="number" class="form-control" name="txt_order" value="0">
					            		@if ($errors->first('txt_order'))
			            					<p class="text-danger">{{ $errors->first('txt_order') }}</p>
			            				@endif
					            	</div>
				            	</div>
				            </div>
				            <div class="form-group">
				            	<div class="row">
				            		<label class="col-md-3">Status</label>
				            		<div class="col-md-6">
				            			<div class="custom-control custom-checkbox">
	                                        <input type="hidden" name="txt_status" value="0">
	                                        <input class="custom-control-input" type="checkbox" id="customCheckbox1" value="1" name="txt_status">
	                                        <label for="customCheckbox1" class="custom-control-label">Enable the slider</label>
	                                    </div>
				            		</div>
				            	</div>
				            </div>			            	
		            	</div>
	        			<!-- /.card-body -->
	        			<div class="card-footer">
		            		<div class="row">
		            			<div class="col-md-6 offset-md-3">				            	
				            		<button type="submit" class="btn btn-success float-right ml-2">Save <i class="fa fa-save"></i></button>
				            		<a href="{{ route('admin.category.index') }}" class="btn btn-danger float-right"><i class="fa fa-undo"></i> Cancle</a>
				            	</div>
			            	</div>
		            	</div>
	            	</form>
		            
	          </div>
			<!-- /.card -->
			</div>
		</div>
	</div>
</section><!-- /.content -->
		
@endsection