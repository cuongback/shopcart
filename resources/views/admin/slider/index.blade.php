@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Slider</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Slider</a></li>
            <li class="breadcrumb-item active">List</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">

	<!-- Default box -->
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">List Slider</h3>

			<div class="card-tools">
				<a href="{{route('admin.slider.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Create Slider</a>
			</div>
		</div>
		<div class="card-body">
			@include('admin.flash-message.flash-message')
			
            <table id="slider" class="table table-bordered table-striped">
                <thead>
	                <tr>
	                	<th>#</th>
	                  	<th>Image</th>
	                  	<th>Title</th>
	                  	<th>Link</th>
	                  	<th>Status</th>
	                  	<th>Order</th>
	                  	<th>Created At</th>
	                  	<th>Action</th>
	                </tr>
                </thead>
                               
            </table>
            
		</div>
		<!-- /.card-body -->
		<div class="card-footer">
			Footer
		</div>
		<!-- /.card-footer-->
	</div>
	<!-- /.card -->

</section>
<!-- /.content -->
@endsection

@section('scripts')
<script>
	$(document).ready(function() {
	    $('#slider').DataTable({
	        processing: true,
	        serverSide: true,
	        responsive: true,
	        ajax: '{{ route('admin.slider.list') }}',
	        columns: [
	        	{
	                data: 'DT_RowIndex',
	                name: 'DT_RowIndex'
	            },
	            {
	                data: 'image',
	                name: 'image'
	            },
	            {
	            	data: 'title',
	            	name: 'title'
	            },
	            {
	                data: 'link',
	                name: 'link'
	            },
	            {
	                data: 'status',
	                name: 'status'
	            },
	            {
	                data: 'order',
	                name: 'order'
	            },
	            {
	                data: 'created_at',
	                name: 'created_at'
	            },
	            {
	                data: 'action',
	                name: 'action',
	                orderable: false,
	                searchable: false
	            }
	        ]
	    });
	});
</script>

@endsection