@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Products</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Products</a></li>
					<li class="breadcrumb-item active">New</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
	<!-- Default card -->
	<div class="row">
		<div class="col-12">
			<div class="card">
	            <div class="card-header">
	              <h3 class="card-title">Danh sách sản phẩm</h3>
	              <div class="card-tools">
	              	<a href="{{ route('admin.product.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create Product</a>
	              </div>
	            </div>
            	<!-- /.card-header -->
	            <div class="card-body">
	            	@include('admin.flash-message.flash-message')
	            	
		            <table id="product" class="table">
		                <thead class="table-primary">
			                <tr>
			                	<th>#</th>
			                	<th>Image</th>
			                  	<th>Name</th>
			                  	<th>Category</th>
			                  	<th>Price</th>
			                  	<th>Status</th>
			                  	<th>Created</th>
			                  	<th>Action</th>
			                </tr>
		                </thead>             
		            </table>
	            </div>
            	<!-- /.box-body -->
          </div>
		<!-- /.box -->
		</div>
	</div>
</section><!-- /.content -->
@endsection

@section('scripts')
<script>
	$(document).ready(function() {
	    $('#product').DataTable({
	        processing: true,
	        serverSide: true,
	        responsive: true,
	        ajax: '{{ route('admin.product.list') }}',
	        columns: [
	        	{
	                data: 'DT_RowIndex',
	                name: 'DT_RowIndex'
	            },
	            {
	                data: 'pro_avatar',
	                name: 'pro_avatar',
	                orderable: false,
	                searchable: false
	            },
	            {
	            	data: 'pro_name',
	            	name: 'pro_name'
	            },
	            {
	                data: 'pro_category_id',
	                name: 'pro_category_id'
	            },
	            {
	                data: 'pro_price',
	                name: 'pro_price'
	            },
	            {
	                data: 'pro_active',
	                name: 'pro_active'
	            },
	            {
	                data: 'created_at',
	                name: 'created_at'
	            },
	            {
	                data: 'action',
	                name: 'action',
	                orderable: false,
	                searchable: false
	            }
	        ]
	    });
	});
</script>

@endsection