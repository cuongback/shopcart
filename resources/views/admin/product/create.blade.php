@extends('admin.layout.master-admin')
<!-- Content Header (Page header) -->
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Product</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Product</a></li>
          <li class="breadcrumb-item active">New</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="row">
    <div class="col-12">
      <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 border-bottom-0">
          <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Product Information</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="custom-tabs-three-gallery-tab" data-toggle="pill" href="#custom-tabs-three-gallery" role="tab" aria-controls="custom-tabs-three-gallery" aria-selected="false">Product gallery</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Specification</a>
            </li>
          </ul>
        </div>
        <div class="card-body">
          <div class="tab-content" id="custom-tabs-three-tabContent">
            <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
              <form role="form" id="add-product" method="POST" action="" enctype="multipart/form-data">
                  @csrf
                  <div class="row">
                    <div class="col-md-8">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Basic Information</h3>
                            </div>
                            <div class="card-body">
                               <div class="form-group">
                                 <label for="nameProduct">Name <span class="text-danger">*</span></label>
                                 <input type="text" class="form-control" name="txt_proName" placeholder="Tên sản phẩm" autocomplete="off">
                               </div>
                               <div class="row">
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="priceEntry">Entry price <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" name="txt_proPriceEntry" placeholder="Giá nhập vào">
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="price">Price <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" name="txt_proPrice" placeholder="Giá sản phẩm">
                                    </div>
                                  </div>
                               </div>
                               <div class="row">
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="priceEntry">Sale (%) <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" name="txt_proSale" placeholder="Giảm giá">
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="price">Sale price <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" name="txt_proPriceSale" placeholder="Giá sale">
                                    </div>
                                  </div>
                               </div>
                               <div class="form-group">
                                  <label for="description">Description <span class="text-danger">*</span></label>
                                  <textarea name="txt_proDescription" id="description" class="textarea form-control" placeholder="Mô tả sản phẩm"
                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                               </div>
                               <div class="form-group">
                                 <label for="category">Category <span class="text-danger">*</span></label>
                                 <select class="form-control select2" name="txt_cateId">
                                   <option value="" disabled selected>Choose your option</option>
                                   @php
                                      echo $optionCate;
                                   @endphp
                                 </select>
                               </div>
                               <div class="form-group">
                                 <label for="content">Content</label>
                                 <textarea name="txt_proContent" id="content" class="content" class="textarea form-control"></textarea>
                               </div>
                               <div class="form-group">
                                  <div class="row">
                                    <label for="status" class="mr-3">Status</label>                                 
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="txt_status" value="0">
                                        <input class="custom-control-input" type="checkbox" id="customCheckbox1" value="1" name="txt_status">
                                        <label for="customCheckbox1" class="custom-control-label">Enable the product</label>
                                      </div>
                                  </div>                               
                                </div>
                            </div>
                            <div class="card-footer">
                              <div class="row">
                                <div class="col-12">
                                  <button type="submit" class="btn btn-success float-right ml-2">Save <i class="fa fa-save"></i></button>
                                  <a href="{{ route('admin.category.index') }}" class="btn btn-danger float-right"><i class="fa fa-undo"></i> Cancle</a>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card card-primary">
                        <div class="card-header">
                          <div class="card-title">Featured Image</div>
                        </div>
                        <div class="card-body">
                          <div class="image-featured">
                            <div class="image-preview">
                                <div class="image-preview-border">
                                  <i class="far fa-image" aria-hidden="true"></i>
                                </div>
                                <label class="btn btn-primary w-100" style="margin: 0;border-radius: 0;">Upload<input type="file" name="txt_image" class="img-file" style="
                                    width: 0;
                                    height: 0;
                                    overflow: hidden;
                                "></label>
                            </div>
                          </div>
                        </div>
                      </div>                    
                    </div>
                  </div>
              </form>
              
            </div>
              <!-- /.tab-pane -->
              <div class="tab-pane fade" id="custom-tabs-three-gallery" role="tabpanel" aria-labelledby="custom-tabs-three-gallery-tab">
                <!-- <div class="card card-primary">
                  <div class="card-header">
                    <div class="card-title">Image Gallery</div>
                  </div>
                  <div class="card-body">
                    <div class="row image-thumnail">
                      <div class="col-md-4 image-thumnail-default">
                        <div class="image-gallery mb-3">
                          <div class="image-gallery-border">
                            <i class="far fa-image" aria-hidden="true"></i>
                          </div>
                        </div>
                      </div>                            
                    </div>
                    <div class="row">
                      
                    </div>
                  </div>
                </div> -->
                <form action="" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <div class="needsclick dropzone" id="gallery-dropzone">

                        </div>
                    </div>
                    <div>
                        <input class="btn btn-success" type="submit">
                    </div>
                </form>
              <!-- /.tab-pane -->
            </div>
            <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                No Data Available
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->

          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('scripts')
<script>
  var uploadedGalleryMap = {};
  Dropzone.options.galleryDropzone = {
    url: '{{ route('admin.image.create') }}',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="gallery[]" value="' + response.name + '">');
      uploadedGalleryMap[file.name] = response.name;
    },
    removedfile: function (file) {
      file.previewElement.remove();
      var name = '';
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name;
      } else {
        name = uploadedGalleryMap[file.name];
      }
      $('form').find('input[name="gallery[]"][value="' + name + '"]').remove();
    }
  }
</script>
@endsection