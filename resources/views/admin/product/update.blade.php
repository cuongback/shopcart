@extends('admin.layout.master-admin')
<!-- Content Header (Page header) -->
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Product</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Product</a></li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
    @include('admin.flash-message.flash-message')
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="row">
    <div class="col-12">
      <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 border-bottom-0">
          <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Product Information</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="custom-tabs-three-gallery-tab" data-toggle="pill" href="#custom-tabs-three-gallery" role="tab" aria-controls="custom-tabs-three-gallery" aria-selected="false">Product gallery</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Specification</a>
            </li>
          </ul>
        </div>
        <div class="card-body">
          <div class="tab-content" id="custom-tabs-three-tabContent">
            <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
              <form role="form" id="add-product" method="POST" action="" enctype="multipart/form-data">
                  @csrf
                  <div class="row">
                    <div class="col-md-8">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Basic Information</h3>
                            </div>
                            <div class="card-body">
                               <div class="form-group">
                                 <label for="nameProduct">Name <span class="text-danger">*</span></label>
                                 <input type="text" class="form-control" name="txt_proName" placeholder="Tên sản phẩm" autocomplete="off" value="{{$product->pro_name}}">
                               </div>
                               <div class="row">
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="priceEntry">Entry price <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" name="txt_proPriceEntry" placeholder="Giá nhập vào" value="{{$product->pro_price_entry}}">
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="price">Price <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" name="txt_proPrice" placeholder="Giá sản phẩm" value="{{$product->pro_price}}">
                                    </div>
                                  </div>
                               </div>
                               <div class="row">
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="priceEntry">Sale (%) <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" name="txt_proSale" placeholder="Giảm giá" value="{{$product->pro_sale}}">
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="price">Sale price <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" name="txt_proPriceSale" placeholder="Giá sale" value="{{$product->pro_price_sale}}">
                                    </div>
                                  </div>
                               </div>
                               <div class="form-group">
                                  <label for="description">Description <span class="text-danger">*</span></label>
                                  <textarea name="txt_proDescription" id="description" class="form-control" placeholder="Mô tả sản phẩm"
                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$product->pro_description}}</textarea>
                               </div>
                               <div class="form-group">
                                 <label for="category">Category <span class="text-danger">*</span></label>
                                 <select class="form-control select2" name="txt_cateId">
                                   <option value="" disabled selected>Choose your option</option>
                                   @php
                                      echo $listCate;
                                   @endphp
                                 </select>
                               </div>
                               <div class="form-group">
                                 <label for="content">Content</label>
                                 <textarea name="txt_proContent" id="content" class="content" class="textarea form-control">{{$product->pro_content}}</textarea>
                               </div>
                               <div class="form-group">
                                  <div class="row">
                                    <label for="status" class="mr-3">Status</label>                                 
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="txt_status" value="0">
                                        <input class="custom-control-input" type="checkbox" id="customCheckbox1" value="1" name="txt_status" {{$product->pro_active == 1 ? 'checked': ''}}>
                                        <label for="customCheckbox1" class="custom-control-label">Enable the product</label>
                                      </div>
                                  </div>                               
                                </div>
                            </div>
                            <div class="card-footer">
                              <div class="row">
                                <div class="col-12">
                                  <button type="submit" class="btn btn-success float-right ml-2">Save <i class="fa fa-save"></i></button>
                                  <a href="{{ route('admin.category.index') }}" class="btn btn-danger float-right"><i class="fa fa-undo"></i> Cancle</a>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card card-primary">
                        <div class="card-header">
                          <div class="card-title">Featured Image</div>
                        </div>
                        <div class="card-body">
                          <div class="image-featured">
                            <div class="image-preview">
                              @if(empty($product->pro_avatar))
                                <div class="image-preview-border">
                                  <i class="far fa-image" aria-hidden="true"></i>
                                </div>
                                <label class="btn btn-primary w-100" style="margin: 0;border-radius: 0;">Upload<input type="file" name="txt_image" class="img-file" style="
                                    width: 0;
                                    height: 0;
                                    overflow: hidden;
                                "></label>
                              @else
                                <div class="image-preview-border">
                                  <img src="{{ pare_url_file($product->pro_avatar, 'products', 'small') }}" alt="{{$product->pro_avatar}}">
                                </div>
                                <a href="{{ route('admin.product.image.delete', $product->id) }}" class="btn btn-primary w-100" style="margin: 0;border-radius: 0;">Remove</a>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="card card-primary">
                      <div class="card-header">
                        <div class="card-title">Image Gallery</div>
                      </div>
                        <div class="card-body">
                          <div class="row image-thumnail">
                            @if(count($product->images))
                              @foreach($product->images as $img)
                              <div class="col-md-4 image-thumnail-default">
                                <div class="image-gallery mb-3">
                                  <div class="image-gallery-border">
                                    <img src="{{ pare_url_file($img->image, 'products', 'small')}}" alt="{{ $img->image }}">
                                    <span class="close-image" rel="{{$img->id}}">x</span>
                                  </div>
                                </div>
                              </div>
                              @endforeach
                            @else
                            <div class="col-md-4 image-thumnail-default">
                              <div class="image-gallery mb-3">
                                <div class="image-gallery-border">
                                  <i class="far fa-image" aria-hidden="true"></i>
                                </div>
                              </div>
                            </div>
                            @endif                           
                          </div>                          
                        </div>
                      </div>
                    </div>
                  </div>
              </form>
            </div>
            <div class="tab-pane fade" id="custom-tabs-three-gallery" role="tabpanel" aria-labelledby="custom-tabs-three-gallery-tab">
                <form action="{{ route('admin.image.store', $product->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <div class="needsclick dropzone" id="gallery-dropzone">

                        </div>
                    </div>
                    <div>
                        <input class="btn btn-success" type="submit">
                    </div>
                </form>
              <!-- /.tab-pane -->
            </div>
              <!-- /.tab-pane -->
            <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                <form action="{{ route('admin.productAttribute.create', $product->id) }}" method="POST" class="form-horizontal">
                    @csrf
                    <div class="table-responsive product-attribute-wrapper">
                      <table class="table-attribute border">
                        <thead class="border-bottom">
                          <tr>
                            <th></th>
                            <th>Attribute</th>
                            <th>Value</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          @if(!count($product->attributes))
                          <tr>
                            <td class="drag-attribute"><i class="fas fa-bars"></i></td>
                            <td class="px-2" style="width: 200px;">
                              <select class="form-control attribute-group" name="attributes[0][attribute_id]" data-attribute-id="0" required>
                                <option value="">Please select</option>
                                @if(count($attributeGroup))
                                  @foreach($attributeGroup as $attVal)
                                  <optgroup label="{{$attVal->att_name}}">
                                    @foreach($attVal->attributes as $val)
                                      <option value="{{$val->id}}" data-values='@json($val->values->pluck('atb_val_name', 'id'), JSON_FORCE_OBJECT)'>{{$val->atb_name}}</option>
                                    @endforeach
                                  </optgroup>
                                  @endforeach
                                @endif
                              </select>
                            </td>
                            <td class="px-2">
                              <select class="selectize" name="attributes[0][values][]" id="attribute.0.values" required multiple>

                              </select>
                            </td>
                            <td class="trash-attribute"><button class="btn btn-default btn-remove" title="Delete Value"><i class="fa fa-trash"></i></button></td>
                          </tr>
                          @else
                            @foreach($product->attributes as $key => $attr)
                              <tr>
                                <td class="drag-attribute"><i class="fas fa-bars"></i></td>
                                <td class="px-2" style="width: 200px;">
                                  <select class="form-control attribute-group" name="attributes[{{$key}}][attribute_id]" data-attribute-id="{{$key}}" required>
                                    <option value="">Please select</option>
                                    @if(count($attributeGroup))
                                      @foreach($attributeGroup as $attVal)
                                      <optgroup label="{{$attVal->att_name}}">
                                        @foreach($attVal->attributes as $val)
                                          <option value="{{$val->id}}" data-values='@json($val->values->pluck('atb_val_name', 'id'), JSON_FORCE_OBJECT)' {{($attr->attribute_id == $val->id) ? 'selected' : ''}}> {{$val->atb_name}} </option>
                                        @endforeach
                                      </optgroup>
                                      @endforeach
                                    @endif
                                  </select>
                                </td>
                                <td class="px-2">
                                  <select class="selectize" name="attributes[{{$key}}][values][]" id="attribute.{{$key}}.values" required multiple>
                                    @foreach($attr->values as $attrValue)
                                      <option value="{{ $attrValue->attribute_value_id }}" selected>{{$attrValue->getValueAttribute()}}</option>
                                    @endforeach
                                  </select>
                                </td>
                                <td class="trash-attribute"><button class="btn btn-default btn-remove" title="Delete Value"><i class="fa fa-trash"></i></button></td>
                              </tr>
                            @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                    <div class="attribute-btn d-flex flex-column align-items-start">
                      <a href="javascript:void(0)" class="btn btn-default my-3 btn-add-value">Add New Value</a>
                      <button class="btn btn-primary">Save</button>
                    </div>
                </form> 
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('scripts')
<script>
  var uploadedGalleryMap = {};
  Dropzone.options.galleryDropzone = {
    url: '{{ route('admin.image.create') }}',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="gallery[]" value="' + response.name + '">');
      uploadedGalleryMap[file.name] = response.name;
    },
    removedfile: function (file) {
      file.previewElement.remove();
      var name = '';
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name;
      } else {
        name = uploadedGalleryMap[file.name];
      }
      $('form').find('input[name="gallery[]"][value="' + name + '"]').remove();
    }
  }
</script>
@endsection