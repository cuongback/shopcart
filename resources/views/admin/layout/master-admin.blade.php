<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Trang quản trị | Shop zomart</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" /> 
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Selectize -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
  <!-- Dropzone -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.css" integrity="sha256-AgL8yEmNfLtCpH+gYp9xqJwiDITGqcwAbI8tCfnY2lw=" crossorigin="anonymous" />
  <!-- Sweet Alert -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
  <style type="text/css">
    .high {
        font-weight: bold;
    }
    .category-child ul > li {
        display: block;
        background: #e6e6e6;
        padding: 8px 8px 6px 25px;
        border: 1px solid #d2d6de;
        border-radius: 4px;
        margin-bottom: 8px;
    }
    .category-child-btn {
        float: right;
    }
    .category-child-title {
        vertical-align: middle;
        font-weight: bold;
        font-family: 'Tahoma';
        letter-spacing: .5px;
        font-size: 14px;
    }
    .fix-tab {
        padding: 8px 0 8px 8px;
        background-color: #f6f6f7
    }
    .fix-tab > .nav-link {
        color: inherit !important;
    }
    .fix-tab > .nav-link.active {
        border-left: 4px solid;
        border-left-color: #007bff !important;
    }
    .tab-fix {
      padding-left: 1rem;
    }
    .tab-fix h4 {
      border-bottom: 1px solid #dee2e6;
      padding-bottom: 14px;
      margin-bottom: 20px;
    }
    .table-attribute {
       width: 100%;
       color: #333;
    }
    .table-attribute td {
      border: 1px solid #dee2e6;
    }
    .drag-attribute {
      width: 40px;
      text-align: center;
    }
    .trash-attribute {
      padding: 8px;
      width: 58px;
    }
    .trash-attribute .btn-remove {
        border-color: #f8f9fa;
    }
    .trash-attribute .fa-trash {        
        font-size: 13px;
    }
    .drag-attribute .fa-bars {
      font-size: 13px;
    }
    .image-featured {
      display: flex;
      justify-content: center;
      align-items: center;
    }
    .image-featured .fa-image {
      font-size: 100px;
      color: #dee2e6;
    }
    .image-preview {
        width: 50%;
        height: auto;
        text-align: center;
        padding: 8px;
        background: #f1f1f1;
        position: relative;
    }
    .image-preview-border {
      border: 1px solid #e9ecef;
      background: #fff;
      height: 200px;
      display: flex;
      justify-content: center;
      align-items: center;
      overflow: hidden;
    }
    .image-preview-border > img {
      max-width: 100%;
      max-height: 100%;
    }
    .tox .tox-notification--in {
      display: none !important;
    }
    .image-gallery {
      background: #f1f1f1;
      padding: 8px;
    }
    .image-gallery-border {
      border: 1px solid #e9ecef;
      background: #fff;
      height: 134px;
      position: relative;
      overflow: hidden;
    }
    .image-gallery-border i {
      font-size: 60px;
      color: #dee2e6;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }
    .image-gallery-border img {
      max-width: 100%;
      height: 100%;
      object-fit: cover;
    }
    .close-image {
      position: absolute;
      font-weight: bold;
      font-size: 20px;
      top: -6px;
      right: 0;
      margin-right: 6px;
      cursor: pointer;
      display: none;
      z-index: 999;
    }
    .image-gallery:hover img {
      opacity: .5;
    }
    .image-gallery:hover .close-image {
      display: block;
    }
    .dropzone {
      border: 1px dashed #d2d6de;
      min-height: 230px;
    }
    .dropzone .dz-message {
      font-size: 20px;
      color: #646c7f;
      margin-top: 70px;
    }
    .table-responsive {
      overflow-x: initial;
    }
    .selectize-control.multi .selectize-input>div.active {
      background: #efefef;
      color: #333;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  @include('admin.header.header')

  @include('admin.sidebar.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->
  @include('admin.footer.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('admin/plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('admin/plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('admin/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('admin/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('admin/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('admin/dist/js/demo.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('admin/js/script-admin.js') }}"></script>
<!-- Tinymce -->
<script src="https://cdn.tiny.cloud/1/apiKey=5g5faf78gvk6yfq9bd3bbfjo858kjx1q8o0nbiwtygo2e4er/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<!-- Jquery Validate -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('admin/js/validation-form.js')}}"></script>
<!-- Dropzon -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js" integrity="sha256-OG/103wXh6XINV06JTPspzNgKNa/jnP1LjPP5Y3XQDY=" crossorigin="anonymous"></script>
<!-- Sweet Alert -->
<script src="{{ asset('admin/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Selectize -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/selectize.min.js" integrity="sha256-zwkv+PhVN/CSaFNLrcQ/1vQd3vviSPiOEDvu2GxYxQc=" crossorigin="anonymous"></script>
<script>
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>
@yield('scripts')
</body>
</html>
