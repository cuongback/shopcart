@if ($message = Session::get('success'))
	<div class="alert alert-success alert-dismissible fade in show">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Success!</h4>
        {{ $message }}.
    </div>
@endif

@if ($message = Session::get('error'))
	<div class="alert alert-danger alert-dismissible fade in show">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Error!</h4>
        {{ $message }}.
    </div>
@endif

@if ($errors->any())
	<div class="alert alert-danger alert-dismissible fade in show">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Error!</h4>
        @foreach ($errors->all() as $error)
            <span>{{ $error }}</span>
        @endforeach
    </div>
@endif