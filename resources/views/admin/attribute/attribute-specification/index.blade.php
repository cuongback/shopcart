@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Attribute</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Attribute</a></li>
            <li class="breadcrumb-item active">List</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
	<!-- Default card -->
	<div class="row">
		<div class="col-12">
			<div class="card">
	            <div class="card-header">
	              <h3 class="card-title">List Attributes</h3>
	              <div class="card-tools">
	              	<a href="{{ route('admin.attribute.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create Attribute</a>
	              </div>
	            </div>
            	<!-- /.card-header -->
	            <div class="card-body">
	            	@include('admin.flash-message.flash-message')	            	
	            	
		            <table id="attribute" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                	<th>#</th>
			                	<th>Attribte group</th>
			                  	<th>Name</th>			                  	
			                  	<th>Filterable</th>
			                  	<th>Action</th>
			                </tr>
		                </thead>
		                               
		            </table>
		            
	            </div>
            	<!-- /.box-body -->
          </div>
		<!-- /.box -->
		</div>
	</div>
</section><!-- /.content -->
		
@endsection

@section('scripts')
	<script>
		$(document).ready(function() {
			$('#attribute').DataTable({
				processing: true,
	        	serverSide: true,
	        	responsive: true,
	        	ajax: '{{ route('admin.attribute.list') }}',
	        	columns: [
		        	{
		                data: 'DT_RowIndex',
		                name: 'DT_RowIndex'
		            },
		            {
		            	data: 'atb_group_id',
		            	name: 'atb_group_id'
		            },
		            {
		                data: 'atb_name',
		                name: 'atb_name'
		            },		            
		            {
		                data: 'atb_fillable',
		                name: 'atb_fillable'
		            },
		            {
		                data: 'action',
		                name: 'action',
		                orderable: false,
		                searchable: false
		            }
		        ]
			});
		});
	</script>
@endsection