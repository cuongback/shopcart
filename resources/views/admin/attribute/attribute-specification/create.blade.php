@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Attribute</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Attribute</a></li>
					<li class="breadcrumb-item active">New</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
	<!-- Default card -->	
	<div class="row">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title">Create Attribute</h3>
				</div>   
				<div class="card-body">					
					<div class="row">
						<div class="col-3">
							<h4>Attribute Information</h4>
							<div class="nav flex-column nav-tabs fix-tab" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
								<a class="nav-link active" id="vert-tabs-general-tab" data-toggle="pill" href="#vert-tabs-general" role="tab" aria-controls="vert-tabs-general" aria-selected="true">General</a>
								<a class="nav-link" id="vert-tabs-value-tab" data-toggle="pill" href="#vert-tabs-value" role="tab" aria-controls="vert-tabs-value" aria-selected="false">Values</a>
							</div>
						</div>
						<div class="col-9">
							<div class="tab-content" id="vert-tabs-tabContent">
								<div class="tab-pane tab-fix text-left fade show active" id="vert-tabs-general" role="tabpanel" aria-labelledby="vert-tabs-general-tab">
									<form action="" method="POST" class="form-horizontal">
										@csrf
										<h4>General</h4>
										<div class="form-group">
											<div class="row">
												<label for="attribute-group" class="col-md-3">Attribute Group <span class="text-danger">*</span></label>
												<div class="col-md-6">
													<select class="form-control {{ $errors->first('txt_attributeGroup') ? 'is-invalid' : ''}}" name="txt_attributeGroup">
														<option disabled selected>--Select choose option--</option>
														@foreach($categories as $cate)
															 <optgroup label="{{ $cate->c_name}}">
															 	@foreach($attributeGroup as $attr)
															 		@if($cate->att_category_id == $attr->att_category_id)
															 			<option value="{{$attr->id}}">
															 				{{$attr->att_name}}
															 			</option>
															 		@endif
															 	@endforeach
															 </optgroup>
														@endforeach
													</select>
													@if ($errors->first('txt_attributeGroup'))
						            					<p class="text-danger">{{ $errors->first('txt_attributeGroup') }}</p>
						            				@endif
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label for="attribute-name" class="col-md-3">Name <span class="text-danger">*</span></label>
												<div class="col-md-6">
													<input type="text" name="txt_attributeName" class="form-control {{ $errors->first('txt_attributeName') ? 'is-invalid' : '' }}" placeholder="Name" autocomplete="off">
													@if ($errors->first('txt_attributeName'))
						            					<p class="text-danger">{{ $errors->first('txt_attributeName') }}</p>
						            				@endif
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<label for="attribute-filter" class="col-md-3">Filterable</label>
												<div class="col-md-6">
													<div class="custom-control custom-checkbox">
							                          <input class="custom-control-input" type="checkbox" id="customCheckbox1" value="option1">
							                          <label for="customCheckbox1" class="custom-control-label">Use this attribute for filtering products</label>
							                        </div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6 offset-md-3">
												<button type="submit" class="btn btn-success">Save <i class="fa fa-save"></i></button>
											</div>
										</div>
									</form>
								</div>
								<div class="tab-pane tab-fix fade" id="vert-tabs-value" role="tabpanel" aria-labelledby="vert-tabs-value-tab">
									<form action="" method="POST" class="form-horizontal">
										@csrf
										<h4>Values</h4>
										<div class="table-responsive">
											<table class="table-attribute border">
												<thead class="border-bottom">
													<tr>
														<th></th>
														<th>Value</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="drag-attribute"><i class="fas fa-bars"></i></td>
														<td class="px-2"><input type="text" autocomplete="off" class="form-control" name="txtAttributeValue[]" required></td>
														<td class="trash-attribute"><button class="btn btn-default btn-remove" title="Delete Value"><i class="fa fa-trash"></i></button></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="attribute-btn d-flex flex-column align-items-start">
											<button class="btn btn-default my-3">Add New Value</button>
											<button class="btn btn-primary">Save</button>
										</div>
									</form> 
								</div>
							</div>
						</div>
					</div>
				</div>         
			</div>
			<!-- /.box -->
		</div>
	</div>
</section><!-- /.content -->

@endsection