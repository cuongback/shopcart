@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Attribute Group</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Attribute group</a></li>
            <li class="breadcrumb-item active">List</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
	<!-- Default card -->
	<div class="row">
		<div class="col-12">
			<div class="card">
	            <div class="card-header">
	              <h3 class="card-title">List Attribute Group</h3>
	              <div class="card-tools">
	              	<a href="{{ route('admin.attributeGroup.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create Attribute Group</a>
	              </div>
	            </div>
            	<!-- /.card-header -->
	            <div class="card-body">
	            	@include('admin.flash-message.flash-message')

		            <table id="attribute-group" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                	<th>#</th>
			                  	<th>Name</th>
			                  	<th>Category</th>
			                  	<th>Order</th>
			                  	<th>Action</th>
			                </tr>
		                </thead>               
		            </table>
	            </div>
            	<!-- /.card-body -->
          </div>
		<!-- /.card -->
		</div>
	</div>
</section><!-- /.content -->
		
@endsection

@section('scripts')
	<script>
		$(document).ready(function() {
			$('#attribute-group').DataTable({
				processing: true,
	        	serverSide: true,
	        	responsive: true,
	        	ajax: '{{ route('admin.attributeGroup.list') }}',
	        	columns: [
		        	{
		                data: 'DT_RowIndex',
		                name: 'DT_RowIndex'
		            },
		            {
		                data: 'att_name',
		                name: 'att_name'
		            },
		            {
		            	data: 'att_category_id',
		            	name: 'category'
		            },
		            {
		                data: 'att_order',
		                name: 'order'
		            },
		            {
		                data: 'action',
		                name: 'action',
		                orderable: false,
		                searchable: false
		            }
		        ]
			});
		});
	</script>
@endsection