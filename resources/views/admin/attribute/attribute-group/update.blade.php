@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Attribute</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Attribute Group</a></li>
            <li class="breadcrumb-item active">Edit</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
	<!-- Default card -->
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
		            <div class="card-header">
		              <h3 class="card-title">Update Attribute Group</h3>
		            </div>
	            	
		            <form method="POST" action="" class="form-horizontal">
		            		@csrf
		            		<div class="card-body">
			            		<div class="form-group">
				            		<div class="row">
				            			<label class="col-md-3">Name <span>(*)</span></label>
				            			<div class="col-md-6">
				            				<input type="text" name="txt_attName" class="form-control {{ $errors->first('txt_attName') ? 'is-invalid' : '' }}" id="categoryName" placeholder="Tên danh mục ..." autocomplete="off" value="{{ $attributeGroup->att_name }}">
				            				@if ($errors->first('txt_attName'))
				            					<p class="text-danger">{{ $errors->first('txt_attName') }}</p>
				            				@endif
				            			</div>
				            		</div>
				            	</div>
				            	<div class="form-group {{ $errors->first('txt_cateId') ? 'has-error' : ''}}">
				            		<div class="row">
				            			<label class="col-md-3">Category</label>
				            			<div class="col-md-6">
				            				<select class="form-control select2" name="txt_cateId">
							                  	<option selected disabled>-- Select choose option --</option>
							                 	@if (isset($listCate))
								                 	@php
								                 		echo $listCate;
								                 	@endphp
							                 	@endif
							                </select>
							                @if ($errors->first('txt_cateId'))
				            					<p class="text-danger">{{ $errors->first('txt_cateId') }}</p>
				            				@endif
				            			</div>
				            		</div>			                
					            </div>
					            <div class="form-group">
				            		<div class="row">
				            			<label class="col-md-3 control-label">Order</label>
				            			<div class="col-md-6">
				            				<input type="number" class="form-control  {{ $errors->first('txt_attOrder') ? 'is-invalid' : ''}}" name="txt_attOrder" value="{{ $attributeGroup->att_order }}">
				            				@if ($errors->first('txt_attOrder'))
				            					<p class="text-danger">{{ $errors->first('txt_attOrder') }}</p>
				            				@endif
				            			</div>
				            		</div>			                
					            </div>			            	
			            	</div>
		        			<!-- /.card-body -->
		        			<div class="card-footer">
			            		<div class="row">
			            			<div class="col-md-6 offset-md-3">				            		
					            		<button type="submit" class="btn btn-success float-right ml-2">Save <i class="fa fa-save"></i></button>
					            		<a href="{{ route('admin.attributeGroup.index') }}" class="btn btn-danger float-right"><i class="fa fa-undo"></i> Cancle</a>
					            	</div>
				            	</div>
			            	</div>
		            	</form>
	          </div>
			<!-- /.box -->
			</div>
		</div>
	</div>
</section><!-- /.content -->
		
@endsection