@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Category</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Category</a></li>
            <li class="breadcrumb-item active">List</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">

	<!-- Default box -->
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">List Categories</h3>

			<div class="card-tools">
				<a href="{{ route('admin.category.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create Category</a>
			</div>
		</div>
		<div class="card-body">
			@include('admin.flash-message.flash-message')

			
            <table id="category" class="table table-bordered table-striped">
                <thead>
	                <tr>
	                	<th>#</th>
	                  	<th>Name</th>
	                  	<th>Parent</th>
	                  	<th>Slug</th>
	                  	<th>Status</th>
	                  	<th>Action</th>
	                </tr>
                </thead>
                               
            </table>
            
		</div>
		<!-- /.card-body -->
		<div class="card-footer">
			Footer
		</div>
		<!-- /.card-footer-->
	</div>
	<!-- /.card -->

</section>
<!-- /.content -->
@endsection

@section('scripts')
<script>
	$(document).ready(function() {
	    $('#category').DataTable({
	        processing: true,
	        serverSide: true,
	        responsive: true,
	        ajax: '{{ route('admin.category.list') }}',
	        columns: [
	        	{
	                data: 'DT_RowIndex',
	                name: 'DT_RowIndex'
	            },
	            {
	                data: 'c_name',
	                name: 'c_name'
	            },
	            {
	            	data: 'c_parentId',
	            	name: 'c_parentId'
	            },
	            {
	                data: 'c_slug',
	                name: 'c_slug'
	            },
	            {
	                data: 'c_status',
	                name: 'c_status'
	            },
	            {
	                data: 'action',
	                name: 'action',
	                orderable: false,
	                searchable: false
	            }
	        ]
	    });
	});
</script>

@endsection