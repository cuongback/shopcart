@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Category</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Category</a></li>
            <li class="breadcrumb-item active">Edit</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
		            <div class="card-header">
		              <h3 class="card-title">Update Category</h3>
		            </div>
	            	<!-- /.box-header -->
		            
	            	<form method="POST" action="" class="form-horizontal" enctype="multipart/form-data">
	            		@csrf
	            		<div class="card-body">
		            		<div class="form-group">
			            		<div class="row">
			            			<label class="col-md-3">Name <span>(*)</span></label>
			            			<div class="col-md-6">
			            				<input type="text" name="c_name" class="form-control {{ $errors->first('c_name') ? 'is-invalid' : '' }}" id="categoryName" placeholder="Tên danh mục ..." autocomplete="off" value="{{ $category->c_name }}">
			            				@if ($errors->first('c_name'))
			            					<p class="text-danger">{{ $errors->first('c_name') }}</p>
			            				@endif
			            			</div>
			            		</div>
			            	</div>
			            	<div class="form-group">
			            		<div class="row">
			            			<label class="col-md-3">Parent</label>
			            			<div class="col-md-6">
			            				<select class="form-control select2" name="c_parentId">
						                  <option value="0">---Root Category---</option>
						                  	@php
						                  		echo $listOption;
						                  	@endphp
						                </select>
			            			</div>
			            		</div>			                
				            </div>
				            <div class="form-group">
				            	<div class="row">
				            		<label class="col-md-3">Description</label>
				            		<div class="col-md-6">
				            			<textarea class="form-control" rows="3" name="c_description">{{$category->c_description}}</textarea>
				            		</div>
				            	</div>
				            </div>
				            <div class="form-group">
				            	<div class="row">
				            		<label class="col-md-3">Avatar</label>
				            		<div class="col-md-6">
				            			@if($category->c_avatar)
				            			<div class="category-avatar">
				            				<img src="{{ pare_url_file($category->c_avatar, 'category') }}" alt="avatar" class="mr-4">
				            				<a href="#">Remove</a>
				            			</div>
				            			@else
				            			<div class="custom-file">
					            			<input type="file" class="custom-file-input" id="exampleInputFile" name="c_avatar">
					            			<label class="custom-file-label" for="exampleInputFile">Choose file</label>
					            		</div>
					            		@endif
				            		</div>
				            	</div>
				            </div>		            	
		            	</div>
	        			<!-- /.box-body -->
	        			<div class="card-footer">
		            		<div class="row">
		            			<div class="col-md-6 offset-md-3">				            		
				            		<button type="submit" class="btn btn-success float-right ml-2" name="btn_edit" value="edit">Save <i class="fa fa-save"></i></button>
				            		<a href="{{ route('admin.category.index') }}" class="btn btn-danger float-right"><i class="fa fa-undo"></i> Cancle</a>
				            	</div>
			            	</div>
		            	</div>
	            	</form>
		            
	          </div>
			<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				@if (count($listCateChild) > 0)
				<div class="card card-default category-child">				
					<div class="card-header">
						<h3 class="card-title">Sắp xếp thứ tự danh mục con</h3>
						<small class="text-red ml-2"><strong>Kéo thả danh mục để thay đổi thứ tự</strong></small>
					</div>
					<form method="POST" action="{{ route('admin.category.order') }}">
						@csrf
						<div class="card-body">
							<ul class="list-unstyled" id="sortable">
								@foreach ($listCateChild as $child)
									<li class="clearfix">
										<span class="category-child-title">{{ $child->c_name }}</span>
										<input type="text" value="{{ $child->id }}" name="order[]" hidden="hidden">
										<div class="category-child-btn">
											<a href="{{ route('admin.category.update', $child->id) }}" class="btn btn-info btn-edit"><i class="fa fa-edit"></i> Edit</a>
											<a href="#" class="btn btn-danger btn-remove"><i class="fa fa-trash"></i> Delete</a>
										</div>
									</li>
								@endforeach						
							</ul>
						</div>
						<div class="card-footer">
							<input type="submit" class="btn btn-success" value="Save"></a>
						</div>
					</form>				
				</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				@if (count($listAttribute) > 0)
				<div class="card card-default category-child">				
					<div class="card-header">
						<h3 class="card-title">Sắp xếp thứ tự thuộc tính</h3>
						<small class="text-red ml-2"><strong>Kéo thả để thay đổi thứ tự</strong></small>
					</div>
					<form method="POST" action="{{ route('admin.attributeGroup.order') }}">
						@csrf
						<div class="card-body">
							<ul class="list-unstyled" id="sortable">
								@foreach ($listAttribute as $attr)
									<li class="clearfix">
										<span class="category-child-title">{{ $attr->att_name }}</span>
										<input type="text" value="{{ $attr->id }}" name="attrOrder[]" hidden="hidden">
										<div class="category-child-btn">
											<a href="{{ route('admin.attributeGroup.update', $attr->id) }}" class="btn btn-info btn-edit"><i class="fa fa-edit"></i> Edit</a>
											<a href="#" class="btn btn-danger btn-remove"><i class="fa fa-trash"></i> Delete</a>
										</div>
									</li>
								@endforeach						
							</ul>
						</div>
						<div class="card-footer">
							<input type="submit" class="btn btn-success" value="Save"></a>
						</div>
					</form>				
				</div>
				@endif
			</div>
		</div>
	</div>
</section><!-- /.content -->
		
@endsection