@extends('admin.layout.master-admin')
@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Category</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Category</a></li>
            <li class="breadcrumb-item active">New</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
	<!-- Default card -->
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
		            <div class="card-header">
		              <h3 class="card-title">Add Category</h3>
		            </div>
	            	<!-- /.card-header -->
		            
	            	<form method="POST" action="" class="form-horizontal" enctype="multipart/form-data">
	            		@csrf
	            		<div class="card-body">
		            		<div class="form-group">
			            		<div class="row">
			            			<label class="col-md-3">Name <span>(*)</span></label>
			            			<div class="col-md-6">
			            				<input type="text" name="c_name" class="form-control {{ $errors->first('c_name') ? 'is-invalid' : '' }}" id="categoryName" placeholder="Tên danh mục ..." autocomplete="off">
			            				@if ($errors->first('c_name'))
			            					<p class="text-danger">{{ $errors->first('c_name') }}</p>
			            				@endif
			            			</div>
			            		</div>
			            	</div>
			            	<div class="form-group">
			            		<div class="row">
			            			<label class="col-md-3">Parent</label>
			            			<div class="col-md-6">
			            				<select class="form-control select2" name="c_parentId">
						                  <option selected="selected" value="0">---Root Category---</option>
						                  @php
						                  		echo $selectCategory;
						                  @endphp
						                </select>
			            			</div>
			            		</div>			                
				            </div>
				            <div class="form-group">
				            	<div class="row">
				            		<label class="col-md-3">Description</label>
				            		<div class="col-md-6">
				            			<textarea class="form-control" rows="3" name="c_description"></textarea>
				            		</div>
				            	</div>
				            </div>
				            <div class="form-group">
				            	<div class="row">
				            		<label class="col-md-3">Avatar</label>
				            		<div class="col-md-6">
				            			<div class="custom-file">
					            			<input type="file" class="custom-file-input" id="exampleInputFile" name="c_avatar">
					            			<label class="custom-file-label" for="exampleInputFile">Choose file</label>
					            		</div>
				            		</div>
				            	</div>
				            </div>			            	
		            	</div>
	        			<!-- /.card-body -->
	        			<div class="card-footer">
		            		<div class="row">
		            			<div class="col-md-6 offset-md-3">				            	
				            		<button type="submit" class="btn btn-success float-right ml-2">Save <i class="fa fa-save"></i></button>
				            		<a href="{{ route('admin.category.index') }}" class="btn btn-danger float-right"><i class="fa fa-undo"></i> Cancle</a>
				            	</div>
			            	</div>
		            	</div>
	            	</form>
		            
	          </div>
			<!-- /.card -->
			</div>
		</div>
	</div>
</section><!-- /.content -->
		
@endsection