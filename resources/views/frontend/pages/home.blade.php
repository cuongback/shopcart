@extends('frontend.layouts.master')

@section('title', 'Trang Chủ')

@section('content')
	<div class="top-content pb-4">
		<div class="container">
			<div class="row no-gutters">
				@include('frontend.components.menu')
				@include('frontend.components.slide')
			</div>
		</div>
	</div>
	@include('frontend.layouts.promotion')
	@include('frontend.layouts.home-product')
@endsection