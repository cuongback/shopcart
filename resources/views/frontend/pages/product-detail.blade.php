@extends('frontend.layouts.master')

@section('title', 'Sản phẩm')

@section('content')
	<div class="category-other-page">
		<div class="container">
			<div class="row no-gutters">
				@include('frontend.components.menu')
			</div>
		</div>
	</div>
	@include('frontend.layouts.product-detail')
@endsection