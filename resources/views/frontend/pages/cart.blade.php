@extends('layouts.master')

@section('title', 'Cart')

@section('content')
	<div class="category-other-page">
		<div class="container">
			<div class="row no-gutters">
				@include('layouts.menu')
			</div>
		</div>
	</div>
	@include('layouts.cart')
@endsection