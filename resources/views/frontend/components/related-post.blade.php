<div class="product-detail-related my-4">
	<div class="row">
		<div class="col-lg-3 d-none d-lg-block product-seeing">
			<div class="product-title">
				<p>Sản phẩm bạn đang xem</p>
			</div>
			@include('frontend.components.product-card')
		</div>
		<div class="col-lg-9">
			<div class="product-title">
				<h4 class="text-uppercase font-weight-bold">Sản phẩm tương tự</h4>
			</div>
			<div class="product-item pt-3">
				<div class="owl-carousel owl-theme product-item-related">
					@foreach($relatedPro as $product)
						@include('frontend.components.product-card')
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>