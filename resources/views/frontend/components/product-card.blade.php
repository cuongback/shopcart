<div class="card product-item-info">
	<div class="product-item-img card-img">
		<a href="{{route('layouts.product.detail', [$product->c_slug, $product->pro_slug])}}" title="{{$product->pro_name}}"><img class="card-img-top" src="{{pare_url_file($product->pro_avatar, 'products', 'small')}}" alt="{{$product->pro_name}}"></a>
	</div>
	<div class="card-body">
		<a href="#" title="{{$product->pro_name}}">
			<h5 class="card-title product-item-title">{{$product->pro_name}}</h5>
		</a>
		@if(Route::currentRouteName() == 'layouts.product.detail')
		<div class="card-text product-item-description">
			{!! $product->pro_description !!}
		</div>
		@else
		<div class="card-text product-item-manufactures">
			<img class="" src="https://dienmaycholon.vn/public/picture/series/dienmay_16.png">
		</div>
		@endif
	</div>
	<div class="card-footer product-item-price">
		<div class="product-item-new-price">
			<span class="font-weight-bold">{{number_format($product->pro_price_sale, 0, ',', '.')}} Đ</span>
			@if($product->pro_sale > 0)
				<span class="product-item-sale">-{{$product->pro_sale}}%</span>
			@endif
		</div>
		<div class="product-item-old-price">
			<span><del>{{number_format($product->pro_price, 0, ',', '.')}} Đ</del></span>
		</div>
	</div>
</div>