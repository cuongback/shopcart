<section class="clearfix main-menu d-none d-lg-block">
	<div class="container">
		<div class="row no-gutters">
			<div class="col-lg-3">
				<div class="main-menu-cate">							
					<h6 class="d-inline-block font-weight-bold"><i class="fa fa-bars" aria-hidden="true"></i>Danh mục sản phẩm</h6>
				</div>
			</div>
			<div class="col-lg-9">
				<nav class="navbar navbar-expand-lg navbar-dark" id="navbar">
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link active" href="#">Trang chủ</a>
							</li>									
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Sản phẩm</a>
								<ul class="dropdown-menu">
									@foreach($categories as $category)
									<li class="{{$category->hasItem() ? 'nav-link dropdown' : ''}}">
										<a class="dropdown-item {{$category->hasItem() ? 'dropdown-toggle' : ''}}" href="#">
											{{$category->c_name}}
										</a>
										@if($category->hasItem())
											<ul class="dropdown-menu">
												@foreach($category->children as $cateChild)
												<li>
													<a class="dropdown-item" href="#">{{$cateChild->c_name}}</a>
												</li>												
												@endforeach
											</ul>
										@endif
									</li>									
									@endforeach											
								</ul>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Tin khuyến mãi</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Kinh nghiệm mua sắm</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Về Zomart</a>
							</li>									
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</section> <!-- end main menu -->