<div class="col-12 col-lg-9">
	<div class="top-silde">
		<div id="carouselHome" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				@foreach($sliders as $slider)
				<li data-target="#carouselHome" data-slide-to="{{$loop->index}}" class="{{$loop->first ? 'active' : ''}}"></li>
				@endforeach			
			</ol>
			<div class="carousel-inner">
				@foreach($sliders as $slider)
				<div class="carousel-item {{$loop->first ? 'active' : ''}}">
			      <img src="{{pare_url_file($slider->image, 'slider')}}" width="800px;" height="390px;" class="d-block" alt="{{$slider->title}}">
			      <div class="carousel-caption d-none d-md-block">
			        <h5>{{$slider->title}}</h5>
			      </div>
			    </div>
				@endforeach
			</div>
			<a class="carousel-control-prev" href="#carouselHome" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselHome" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</div>