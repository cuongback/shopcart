<section class="clearfix header">
	<div class="container">
		<div class="row my-4">
			<div class="col-4 col-md-4 col-lg-3">
				<div class="header-brand d-none d-lg-block">
					<a href="/">
						<img class="img-fluid" src="{{asset('images/logo.png')}}" alt="logo">
					</a>
				</div>
				<div class="menu-cate d-lg-none d-block">
					<i class="fa fa-bars p-0" aria-hidden="true"></i>
				</div>
			</div>
			<div class="col-4 col-md-4 col-lg-5">
				<div class="header-search d-none d-lg-block">
					<form autocomplete="off" class="header-search-form">
						<div class="header-collection-selector">
							<div class="header-search-text">
								<span class="mr-2">Tất cả</span>
								<i class="fa fa-caret-down" aria-hidden="true"></i>
							</div>
							<div class="header-search-list">
								<div class="header-search-item">
									<span>Thiết bị khác</span>
								</div>
								<div class="header-search-item">
									<span>Laptop</span>
								</div>
								<div class="header-search-item">
									<span>Máy tính bảng</span>
								</div>
								<div class="header-search-item">
									<span>Smartphone</span>
								</div>
								<div class="header-search-item">
									<span>Phụ kiện</span>
								</div>
								<div class="header-search-item">
									<span>Túi xách</span>
								</div>
								<div class="header-search-item">
									<span>Thời trang nữ</span>
								</div>
								<div class="header-search-item">
									<span>Thời trang nam</span>
								</div>
								<div class="header-search-item">
									<span>Sản phẩm hot</span>
								</div>
								<div class="header-search-item">
									<span>Sản phẩm mới</span>
								</div>
							</div>
						</div>
						<input type="text" autocomplete="off" name="search" id="search" class="header-search-input" placeholder="Tìm kiếm sản phẩm ...">
						<button class="header-search-button">
							Tìm kiếm
						</button>								
					</form>
				</div>
				<img class="img-fluid d-lg-none d-block" src="{{asset('images/logo.png')}}" alt="logo">
			</div>
			<div class="col-4 col-md-4 col-lg-4 d-flex">
				<div class="header-user d-none d-lg-block">
					<a href="#" class="mt-2 mr-2 float-left">
						<img class="img-fluid header-user-img" src="{{asset('images/user-icon.png')}}">
					</a>
					<div class="header-user-body float-right">
						<p class="mb-0"><a href="{{route('auth.product.login')}}">Đăng nhập</a> & <a href="{{route('auth.product.register')}}">Đăng ký</a></p>
						<p class="mb-0 font-weight-bold">Tài khoản</p>
					</div>
				</div>
				<div class="header-cart d-none d-lg-block">
					<a href="#" class="mt-2 mr-2 float-left"><img class="img-fluid header-cart-img" src="{{asset('images/cart-icon.png')}}"></a>
					<div class="header-cart-body float-right">
						<p class="mb-0"><a href="#">Giỏ hàng</a></p>
						<p class="font-weight-bold mb-0">(0) Sản phẩm</p>
					</div>
				</div>
				<div class="cart-icon d-lg-none d-block">
					<img src="{{asset('images/cart-icon.png')}}" class="cart-img">
					<span class="cart-count">0</span>
				</div>
			</div>					
		</div>
		<div class="row d-lg-none d-block">
			<div class="col-md-8 offset-md-2">
				<form autocomplete="off" class="second-search">
					<input type="text" autocomplete="off" name="search" id="searchFix" class="second-search-input" placeholder="Tìm kiếm ...">
					<button class="second-search-button">
						Tìm kiếm
					</button>
				</form>
			</div>
		</div>
	</div>
</section> <!-- end header -->