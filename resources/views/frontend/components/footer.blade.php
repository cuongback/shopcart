<section class="clearfix footer">
	<div class="container">
		<div class="footer-policy py-4">
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-3">
					<h6 class="footer-policy-title">Thông tin công ty</h6>
					<ul class="list-unstyled d-none d-sm-block">
						<li><a href="#">Giới thiệu công ty</a></li>
						<li><a href="#">Hệ thống siêu thị</a></li>
						<li><a href="#">Phương châm bán hàng</a></li>
						<li><a href="#">Cơ hội nghề nghiệp</a></li>
					</ul>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-3">
					<h6 class="footer-policy-title">Hỗ trợ khách hàng</h6>
					<ul class="list-unstyled d-none d-sm-block">
						<li><a href="#">Trung tâm</a></li>
						<li><a href="#">Bảo hành</a></li>
						<li><a href="#">Liên hệ</a></li>
						<li><a href="#">Tư vấn</a></li>
						<li><a href="#">Đổi trả</a></li>
					</ul>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-3">
					<h6 class="footer-policy-title">Chính sách bảo hành</h6>
					<ul class="list-unstyled d-none d-sm-block">
						<li><a href="#">Bảo trì-Bảo hành-Đổi trả</a></li>
						<li><a href="#">Qui định giao hàng</a></li>
						<li><a href="#">Điều khoản sử dụng</a></li>
						<li><a href="#">Thoả thuận người dùng</a></li>
					</ul>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-3">
					<h6 class="footer-policy-title">Mua hàng online</h6>
					<ul class="list-unstyled d-none d-sm-block">
						<li><a href="#">Câu hỏi thường gặp</a></li>
						<li><a href="#">Hướng dẫn mua hàng online</a></li>
						<li><a href="#">Lợi ích mua hàng online</a></li>
					</ul>
				</div>					
			</div>
		</div>
		<div class="footer-address">
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<p class="font-weight-bold text-uppercase">Công ty cổ phần dịch vụ thương mại Zomart</p>
					<p><span class="font-weight-bold">Hà Nội: </span>Tầng 8 Ladeco, 266 Đội Cấn, Hà Nội</p>
					<p><span class="font-weight-bold">Hotline: </span><a href="tel:02473086880">024 73086880 - </a><span>Email: </span><a href="mailto:support@sapo.vn">support@sapo.vn</a></p>
				</div>
				<div class="col-md-6 col-md-6">
					<p><span class="font-weight-bold">Tp.Hồ Chí Minh: </span>Số 70 Lữ Gia - Phường 15, Quận 11, TP Hồ Chí Minh</p>
					<p><span class="font-weight-bold">Hotline: </span><a href="tel:18006750">1800 6750 - </a><span class="font-weight-bold">Email: </span><a href="mailto:support@sapo.vn">support@sapo.vn</a></p>
				</div>
			</div>
		</div>	
		<div class="footer-copyright text-center">
			<p>&copy; Bản quyền thuộc về <span class="font-weight-bold">Cuong Back Team</span> | Cung cấp bởi <a href="#">Cuong Back</a></p>	
		</div>
	</div>
</section> <!-- end footer -->