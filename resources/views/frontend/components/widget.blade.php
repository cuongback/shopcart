<section class="clearfix widget py-4">
	<div class="container">
		<div class="widget-policy">
			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="widget-quality">
						<img class="widget-image" src="{{asset('images/policy1.png')}}">
						<div class="widget-info">
							<p class="widget-title font-weight-bold">Chất lượng hàng đầu</p>
							<span class="widget-description">Cam kết tất cả sản phẩm chính hãng 100%</span>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="widget-delivery">
						<img class="widget-image" src="{{asset('images/policy2.png')}}">
						<div class="widget-info">
							<p class="widget-title font-weight-bold">Giao hàng siêu nhanh</p>
							<span class="widget-description">Chúng tôi cam kết giao hàng trong 24h</span>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="widget-saving">
						<img class="widget-image" src="{{asset('images/policy3.png')}}">
						<div class="widget-info">
							<p class="widget-title font-weight-bold">Mua hàng tiết kiệm</p>
							<span class="widget-description">Giảm giá & khuyến mại với ưu đãi cực lớn</span>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="widget-support">
						<img class="widget-image" src="{{asset('images/policy4.png')}}">
						<div class="widget-info">
							<p class="widget-title font-weight-bold">Hỗ trợ online 24/7</p>
							<span class="widget-description">Gọi ngay (04) 5580 6780 để được tư vấn</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>