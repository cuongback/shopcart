<div class="product-specification mt-4">
	<div class="product-specification-info">
		@foreach($product->attributeGroup as $attributeSet => $attributes)
		<h3>{{$attributeSet}}</h3>
		<div class="product-specification-table">
			@foreach($attributes as $attr)							
			<div class="product-specification-row">
				<div class="product-specification-item">{{$attr->name}}</div>
				<div class="product-specification-item">{{$attr->values->implode('value', ', ')}}</div>
			</div>
			@endforeach					
		</div>		
		@endforeach
	</div>
	<div class="product-specification-more mt-3">
		<a href="javascript:void(0)" data-toggle="modal" data-target="#attributeProduct">Xem thêm cấu hình chi tiết</a>
	</div>
</div>

<div class="modal fade" id="attributeProduct" tabindex="-1" role="dialog" aria-labelledby="attributeProductTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="attributeProductTitle">{{$product->pro_name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="product-specification">
	        @foreach($product->attributeGroup as $attributeSet => $attributes)
			<h3>{{$attributeSet}}</h3>
			<div class="product-specification-table">
				@foreach($attributes as $attr)							
				<div class="product-specification-row">
					<div class="product-specification-item">{{$attr->name}}</div>
					<div class="product-specification-item">{{$attr->values->implode('value', ', ')}}</div>
				</div>
				@endforeach					
			</div>		
			@endforeach
		</div>
      </div>      
    </div>
  </div>
</div>