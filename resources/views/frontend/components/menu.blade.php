<div class="col-lg-3 d-lg-block">
	<div class="mega-menu-overlay"></div>
	<div class="top-category">
		<h3 class="top-category-title d-block d-lg-none">Danh mục sản phẩm</h3>
		<nav>
			<ul class="nav-cate" id="MainMenu">
				@foreach($categories as $key => $category)
				<li class="nav-item">
					<div class="nav-icon">
						<img class="img-fluid" src="{{pare_url_file($category->c_avatar, 'category')}}">
					</div>
					<a href="#" class="nav-text">
						<span>{{$category->c_name}}</span>
						<p>{{$category->c_description}}</p>
						
						@if(!$category->children->isEmpty())
							<i class="fa fa-angle-right" data-target="#collapse{{$key}}" data-toggle="collapse" aria-expanded="false"></i>
						@endif
					</a>
					@if(!$category->children->isEmpty())
						<ul class="mega-menu clearfix">
							<li class="col-lg-12">
								<ul class="mega-menu-item collapse " id="collapse{{$key}}" data-parent="#MainMenu">
									@foreach($category->children->chunk(6) as $chunk)
										
									<li class="col-lg-4">
										<ul class="mega-menu-sub">
											@foreach($chunk as $subMenu)
												<li><a href="#">{{$subMenu->c_name}}</a></li>	
											@endforeach
										</ul>
									</li>
										
									@endforeach								
								</ul>
							</li>
						</ul>
					@endif
				</li>
				@endforeach

				<!-- Mega menu -->
				<!-- <li  class="nav-item">
					<div class="nav-icon">
						<img class="img-fluid" src="{{asset('images/cate2_icon.png')}}">
					</div>
					<a href="#" class="nav-text">
						<span>Điện thoại thông minh</span>
						<p>TV Led, Loa, DVD, Dàn máy...</p>
					</a>
				</li>
				<li  class="nav-item">
					<div class="nav-icon">
						<img class="img-fluid" src="{{asset('images/cate3_icon.png')}}">
					</div>
					<a href="#" class="nav-text">
						<span>Thời trang</span>
						<p>Quần áo, giày dép, mũ...</p>
						<i class="fa fa-angle-right" data-target="#collapseFas" data-toggle="collapse" aria-expanded="false"></i>
					</a>
					<ul class="mega-menu clearfix">
						<li class="col-lg-12">
							<ul class="mega-menu-item collapse " id="collapseFas" data-parent="#MainMenu">
								<li class="col-lg-3">
									<h3 class="mega-menu-title"><a href="#">Smartphone</a><i class="fa fa-chevron-down d-lg-none d-block" data-target="#collapseTwo" data-toggle="collapse" aria-expanded="true"></i></h3>
									<ul class="mega-menu-sub collapse" id="collapseTwo">
										<li><a href="#">Iphone</a></li>
										<li><a href="#">Samsung</a></li>
										<li><a href="#">Sony</a></li>
										<li><a href="#">Xiaomi</a></li>
									</ul>
								</li>
								<li class="col-lg-3">
									<h3 class="mega-menu-title"><a href="#">Tablet</a><i class="fa fa-chevron-down d-lg-none d-block" data-target="#collapseThree" data-toggle="collapse" aria-expanded="true"></i></h3>
									<ul class="mega-menu-sub collapse" id="collapseThree">
										<li><a href="#">Iphone</a></li>
										<li><a href="#">Samsung</a></li>
										<li><a href="#">Sony</a></li>
										<li><a href="#">Xiaomi</a></li>
									</ul>
								</li>
								<li class="col-lg-3">
									<h3 class="mega-menu-title"><a href="#">Linh kiện</a><i class="fa fa-chevron-down d-lg-none d-block" data-target="#collapseFour" data-toggle="collapse" aria-expanded="true"></i></h3>
									<ul class="mega-menu-sub collapse" id="collapseFour">
										<li><a href="#">Iphone</a></li>
										<li><a href="#">Samsung</a></li>
										<li><a href="#">Sony</a></li>
										<li><a href="#">Xiaomi</a></li>
									</ul>
								</li>
								<li class="col-lg-3">
									<h3 class="mega-menu-title"><a href="#">Phụ kiện</a><i class="fa fa-chevron-down d-lg-none d-block" data-target="#collapseFive" data-toggle="collapse" aria-expanded="true"></i></h3>
									<ul class="mega-menu-sub collapse" id="collapseFive">
										<li><a href="#">Iphone</a></li>
										<li><a href="#">Samsung</a></li>
										<li><a href="#">Sony</a></li>
										<li><a href="#">Xiaomi</a></li>
									</ul>
								</li>
								<li class="col-lg-3">
									<h3 class="mega-menu-title"><a href="#">Ốp điện thoại</a><i class="fa fa-chevron-down d-lg-none d-block" data-target="#collapseSix" data-toggle="collapse" aria-expanded="true"></i></h3>
									<ul class="mega-menu-sub collapse" id="collapseSix">
										<li><a href="#">Iphone</a></li>
										<li><a href="#">Samsung</a></li>
										<li><a href="#">Sony</a></li>
										<li><a href="#">Xiaomi</a></li>
									</ul>
								</li>
								<li class="col-lg-3">
									<h3 class="mega-menu-title"><a href="#">Bao da</a><i class="fa fa-chevron-down d-lg-none d-block" data-target="#collapseSev" data-toggle="collapse" aria-expanded="true"></i></h3>
									<ul class="mega-menu-sub collapse" id="collapseSev">										
										<li><a href="#">Iphone</a></li>
										<li><a href="#">Samsung</a></li>
										<li><a href="#">Sony</a></li>
										<li><a href="#">Xiaomi</a></li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li  class="nav-item">
					<div class="nav-icon">
						<img class="img-fluid" src="{{asset('images/cate4_icon.png')}}">
					</div>
					<a href="#" class="nav-text">
						<span>Máy tính & Laptop</span>
						<p>Asus, Dell, Apple, Mouse...</p>
					</a>
				</li>
				<li  class="nav-item">
					<div class="nav-icon">
						<img class="img-fluid" src="{{asset('images/cate5_icon.png')}}">
					</div>
					<a href="#" class="nav-text">
						<span>Sức khỏe & Sắc đẹp</span>
						<p>Nước hoa, mỹ phẩm, son...</p>
					</a>
				</li>
				<li  class="nav-item">
					<div class="nav-icon">
						<img class="img-fluid" src="{{asset('images/cate6_icon.png')}}">
					</div>
					<a href="#" class="nav-text">
						<span>Nội thất & Gia dụng</span>
						<p>Giường, ghế, bàn, tủ, bếp...</p>
					</a>
				</li> -------Mega menu-->
			</ul>
		</nav>
	</div>
</div>