<div class="product-cart">
	<div class="breadcrumb-info">
		<div class="container">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
					<li class="breadcrumb-item" aria-current="page"><a href="#">Giỏ hàng</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<div class="container">
		<div class="product-cart-info">
			<div class="product-cart-table product-cart-desktop">
				<table class="table table-bordered text-center">
					<thead>
						<tr>
							<th>Hình ảnh</th>
							<th>Tên sản phẩm</th>
							<th>Đơn giá</th>
							<th>Số lượng</th>
							<th>Thành tiền</th>
							<th>Xóa</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><img src="{{ asset('images/tivi1.jpg') }}"></td>
							<td>Iphone 5</td>
							<td class="hl-price">15.600.000₫</td>
							<td>
								<div class="product-cart-calc">
									<div class="product-cart-minus">
										<span>_</span>
									</div>
									<input type="text" value="1" class="product-cart-val">
									<div class="product-cart-plus">
										<span>+</span>
									</div>
								</div>
							</td>
							<td class="hl-price">15.600.000₫</td>
							<td><a href="javascript:;" title="Xóa" class="product-cart-del"></a></td>
						</tr>
						<tr>
							<td><img src="{{ asset('images/tivi1.jpg') }}"></td>
							<td>Iphone 6</td>
							<td class="hl-price">15.600.000₫</td>
							<td>
								<div class="product-cart-calc">
									<div class="product-cart-minus">
										<span>_</span>
									</div>
									<input type="text" value="1" class="product-cart-val">
									<div class="product-cart-plus">
										<span>+</span>
									</div>
								</div>
							</td>
							<td class="hl-price">15.600.000₫</td>
							<td><a href="javascript:;" title="Xóa" class="product-cart-del"></a></td>
						</tr>
					</tbody>
				</table>
				<div class="product-cart-update clearfix">
					<a href="#" class="btn btn-cart-grey btn-cart-continue" title="Tiếp tục mua hàng">Tiếp tục mua hàng</a>
					<input type="submit" name="" class="btn btn-cart-grey btn-cart-remove" value="Xóa toàn bộ đơn hàng" title="Xóa toàn bộ đơn hàng">
				</div>
				<div class="product-cart-price clearfix">
					<span class="text-uppercase sumTotal">Tổng tiền</span>
					<span class="price">31.800.000₫</span>
				</div>
				<div class="product-cart-payment">
					<a href="#" title="Tiến hành đặt hàng">Thanh toán</a>
				</div>
			</div>
			<div class="product-cart-mobile">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="cart-mobile-info media">
								<div class="cart-mobile-img">
									<img src=" {{ asset('images/tivi1.jpg') }}" alt="">
								</div>
								<div class="cart-mobile-body media-body">
									<h5>Samsung Galaxy S20+</h5>
									<span>Giá: <span class="hl-price">15.600.000₫</span></span>
									<div class="cart-mobile-sum">
										<span>Số lượng: </span>
										<div class="cart-mobile-total">
											<span class="cart-mobile-minus"></span>
											<input type="text" name="" value="1" class="cart-mobile-val">
											<span class="cart-mobile-plus"></span>
										</div>
									</div>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
			<!-- <p>Không có sản phẩm nào trong giỏ hàng. Quay lại cửa hàng để tiếp tục mua sắm.</p> -->
		</div>
	</div>
</div>