<!DOCTYPE html>
<html lang="en">
	<head>
		<title>@yield('title')</title>
		<!-- Required meta tags -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}">
		<!-- <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}">
		<link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.default.min.css')}}">
		<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'> -->
		<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha256-UK1EiopXIL+KVhfbFa8xrmAWPeBjMVdvYMYkTAEv/HI=" crossorigin="anonymous" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" integrity="sha256-4hqlsNP9KM6+2eA8VUT0kk4RsMRTeS7QGHIM+MZ5sLY=" crossorigin="anonymous" /> -->
	</head>
	<body>
		<section class="clearfix topbar bg-topbar">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 text-left d-none d-lg-block">
						<span class="topbar-contact py-2 d-inline-block">
							<i class="fa fa-phone" aria-hidden="true"></i>
							Bán hàng trực tuyến: (04) 5580 6780 - (08) 5800 6770
						</span>
					</div>
					<div class="col-lg-6 d-none d-lg-block">
						<ul class="nav justify-content-end">
							<li class="nav-item">
								<a href="#" class="nav-link">Trang Chủ</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">Kinh nghiệm mua sắm</a>
							</li>							
							<li class="nav-item">
								<a href="#" class="nav-link">Liên hệ</a>
							</li>							
						</ul>
					</div>
					<div class="col-12 d-block d-lg-none">
						<ul class="nav justify-content-center">
							<li class="nav-item"><a href="#" class="nav-link">Đăng nhập</a></li>
							<li class="nav-item"><a href="#" class="nav-link">Đăng ký</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section> <!-- end top bar -->

		@include('frontend.components.header')

		@include('frontend.components.main-menu')

		<section class="clearfix main-content">
			@yield("content")
		</section> <!-- content -->
		
		@include('frontend.components.widget')
				
		@include('frontend.components.footer')
		

		<!-- Optional Javascript -->
		<!-- Jquery first, then Popper.js, then Boostrap JS -->
		<!-- <script src="{{asset('js/jquery-3.4.1.slim.min.js')}}"></script>
		<script src="{{asset('js/popper.min.js')}}"></script>
		<script src="{{asset('js/bootstrap.min.js')}}"></script>		
		<script src="{{asset('js/owl.carousel.min.js')}}"></script> -->		
		<script src="{{asset('js/app.js')}}"></script>
		<script src="{{asset('js/bootnavbar.js')}}"></script>
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" integrity="sha256-NXRS8qVcmZ3dOv3LziwznUHPegFhPZ1F/4inU7uC8h0=" crossorigin="anonymous"></script> -->
		<script>
			$('#navbar').bootnavbar();
			$('.nav-cate').megaMenu();
			$('.nav-cate').clickMenu();						
		</script>
	</body>
	
</html>