@foreach($categories as $category)
	@php
		$array = array();
		$array[] = $productHotNew[$category->id];
		$cateSlug = str_replace('-', '', $category->c_slug);
	@endphp
<div class="list-product {{$category->c_slug}}">
	<div class="container pt-4">
		<div class="product-title">
			<h4 class="text-uppercase font-weight-bold d-inline-block">Tầng <a href="#">{{$category->c_name}}</a></h4>
			<div class="product-categories float-right d-none d-lg-block">
				<ul class="list-inline text-right mb-0 product-category">
					<li class="list-inline-item"><a href="#" idx="hover-{{$cateSlug}}1" idy="list-{{$cateSlug}}" class="cate_hover_home active-cate" >Mới và nổi bật</a></li>
					@foreach($category->getCategoryWithLimit() as $cateChild)
						<li class="list-inline-item"><a idx="hover-{{$cateSlug}}{{$cateChild->id}}" class="cate_hover_home" idy="list-{{$cateSlug}}" href="#">{{$cateChild->c_name}}</a></li>
					@endforeach
						<li class="list-inline-item"><a href="#"><strong>Xem thêm</strong></a></li>
				</ul>
			</div>
		</div>
		
		@foreach($array as $element)
			<div class="product-item pt-3 hover-{{$cateSlug}}1 list-{{$cateSlug}}">
				<div class="owl-carousel owl-theme product-item-list">
					@foreach($element as $product)
						@include('frontend.components.product-card')
					@endforeach
				</div>
			</div>
		@endforeach
		@foreach($category->getCategoryWithLimit() as $cateChild)
			<div class="product-item pt-3 hover-{{$cateSlug}}{{$cateChild->id}} list-{{$cateSlug}}" style="display: none;">
				<div class="owl-carousel owl-theme product-item-list">
					@foreach($productCate[$cateChild->id] as $product)
						@include('frontend.components.product-card')
					@endforeach
				</div>
			</div>
		@endforeach
	</div>
</div>
@endforeach