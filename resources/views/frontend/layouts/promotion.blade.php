<div class="tab-product-promotion py-4">
	<div class="container">
		<div class="promotion-title">
			<h4 class="text-uppercase font-weight-bold d-inline-block float-left">Tầng <a href="#">Khuyến mãi</a></h4>
			<span class="d-none d-md-block float-left promotion-desc"> (Có <b>120</b> sản phẩm đang khuyến mãi <a href="#" class="font-weight-bold">Xem thêm</a>)</span>
		</div>	
		<div class="list-promotion pt-3">	
			<div class="card-group owl-carousel owl-theme list-promotion-info">
				<div class="card pro-item">
					<div class="card-img">
						<img data-src="{{asset('images/iphone1.jpg')}}" class="owl-lazy card-img-top pro-item-img" alt="">
					</div>
					<div class="card-body">
						<h5 class="card-title">Điện thoại di động Samsung J200GU</h5>
						<div class="card-text">
							<img class="pro-item-manufactures" src="https://dienmaycholon.vn/public/picture/series/dienmay_16.png" alt="">
						</div>
					</div>
					<div class="card-footer pro-price">
						<div class="pro-item-price">
							<span>7.500.000 Đ</span>
							<span class="pro-item-sale">- 10%</span>
						</div>
						<div class="pro-item-old-price">
							<span><del>8.300.000 Đ</del></span>
						</div>						
					</div>
				</div>
				<div class="card pro-item">
					<div class="card-img">
						<img data-src="{{asset('images/ipad1.jpg')}}" class="owl-lazy card-img-top pro-item-img" alt="">
					</div>
					<div class="card-body">
						<h5 class="card-title">Máy tính Bảng</h5>
						<div class="card-text">
							<img class="pro-item-manufactures" src="https://dienmaycholon.vn/public/picture/series/dienmay_16.png" alt="">
						</div>
					</div>
					<div class="card-footer pro-price">
						<div class="pro-item-price">
							<span>7.500.000 Đ</span>
						</div>
					</div>
				</div>
				<div class="card pro-item">
					<div class="card-img">
						<img data-src="{{asset('images/iphone1.jpg')}}" class="owl-lazy card-img-top pro-item-img" alt="">
					</div>
					<div class="card-body">
						<h5 class="card-title">Iphone 6</h5>
						<div class="card-text">
							<img class="pro-item-manufactures" src="https://dienmaycholon.vn/public/picture/series/dienmay_82.png" alt="">
						</div>
					</div>
					<div class="card-footer pro-price">
						<div class="pro-item-price">
							<span>7.500.000 Đ</span>
						</div>
					</div>
				</div>
				<div class="card pro-item">
					<div class="card-img">
						<img data-src="{{asset('images/iphone1.jpg')}}" class="owl-lazy card-img-top pro-item-img" alt="">
					</div>
					<div class="card-body">
						<h5 class="card-title">Iphone 7</h5>
						<div class="card-text">
							<img class="pro-item-manufactures" src="https://dienmaycholon.vn/public/picture/series/dienmay_82.png" alt="">
						</div>
					</div>
					<div class="card-footer pro-price">
						<div class="pro-item-price">
							<span>7.500.000 Đ</span>
						</div>
					</div>
				</div>
				<div class="card pro-item">
					<div class="card-img">
						<img data-src="{{asset('images/iphone1.jpg')}}" class="owl-lazy card-img-top pro-item-img" alt="">
					</div>
					<div class="card-body">
						<h5 class="card-title">Điện thoại di động LG 3451</h5>
						<div class="card-text">
							<img class="pro-item-manufactures" src="https://dienmaycholon.vn/public/picture/series/dienmay_16.png" alt="">
						</div>
					</div>
					<div class="card-footer pro-price">
						<div class="pro-item-price">
							<span>7.500.000 Đ</span>
							<span class="pro-item-sale">- 10%</span>
						</div>
						<div class="pro-item-old-price">
							<span><del>8.300.000 Đ</del></span>
						</div>						
					</div>
				</div>
				<div class="card pro-item">
					<div class="card-img">
						<img data-src="{{asset('images/iphone1.jpg')}}" class="owl-lazy card-img-top pro-item-img" alt="">
					</div>
					<div class="card-body">
						<h5 class="card-title">Iphone 8</h5>
						<div class="card-text">
							<img class="pro-item-manufactures" src="https://dienmaycholon.vn/public/picture/series/dienmay_82.png" alt="">
						</div>
					</div>
					<div class="card-footer pro-price">
						<div class="pro-item-price">
							<span>7.500.000 Đ</span>
						</div>
					</div>
				</div>
				<div class="card pro-item">
					<div class="card-img">
						<img data-src="{{asset('images/iphone1.jpg')}}" class="owl-lazy card-img-top pro-item-img" alt="">
					</div>
					<div class="card-body">
						<h5 class="card-title">Iphone 9</h5>
						<div class="card-text">
							<img class="pro-item-manufactures" src="https://dienmaycholon.vn/public/picture/series/dienmay_82.png" alt="">
						</div>
					</div>
					<div class="card-footer pro-price">
						<div class="pro-item-price">
							<span>7.500.000 Đ</span>
						</div>
					</div>
				</div>
				<div class="card pro-item">
					<div class="card-img">
						<img data-src="{{asset('images/iphone1.jpg')}}" class="owl-lazy card-img-top pro-item-img" alt="">
					</div>
					<div class="card-body">
						<h5 class="card-title">Iphone X</h5>
						<div class="card-text">
							<img class="pro-item-manufactures" src="https://dienmaycholon.vn/public/picture/series/dienmay_82.png" alt="">
						</div>
					</div>
					<div class="card-footer pro-price">
						<div class="pro-item-price">
							<span>7.500.000 Đ</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>