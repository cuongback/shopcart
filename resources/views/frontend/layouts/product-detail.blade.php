<div class="product product-detail">
	<div class="breadcrumb-info">
		<div class="container">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
					<li class="breadcrumb-item"><a href="#">Di động - Tablet</a></li>
					<li class="breadcrumb-item" aria-current="page"><a href="#">Điện thoại di động</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<div class="container">		
		<div class="product-detail-main product-info">			
			<div class="row">
				<div class="col-lg-12 col-xl-9">
					<div class="row product-detail-info">
						<div class="col-lg-6">
							<div class="card product-info-img base-image">
								<img src="{{pare_url_file($product->pro_avatar, 'products', 'medium')}}" alt="{{$product->pro_name}}" class="img-index">								
							</div>
							<div class="product-info-thumbnails thumnail-image">
								<div class="img-gallery">
									<a class="card active-img" href="javascript:void(0)">
										<img class="img-thum" src="{{pare_url_file($product->pro_avatar, 'products', 'small')}}" alt="{{$product->pro_name}}" val="{{pare_url_file($product->pro_avatar, 'products', 'medium')}}">
									</a>
								</div>
								@foreach($product->images as $img)
									<div class="img-gallery">
										<a class="card" href="javascript:void(0)">
											<img class="img-thum" src="{{pare_url_file($img->image, 'products', 'small')}}" alt="" val="{{pare_url_file($img->image, 'products', 'medium')}}">
										</a>
									</div>									
								@endforeach
							</div>
						</div>
						<div class="col-lg-6">
							<h3 class="product-info-title">{{$product->pro_name}}</h3>
							<div class="product-info-brand pb-2">
								<span>Thương hiệu: </span><a href="#">Apple</a>
								<span>Mã sản phẩm: <strong>{{$product->id}}</strong></span>
							</div>
							<div class="product-info-rating">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
								<a href="#">Có 200 đánh giá | Có 30 câu trả lời</a>
							</div>
							<h2 class="product-info-price">{{number_format($product->pro_price_sale, 0, ',', '.')}}₫</h2>
							<div class="product-info-gift mt-3 pb-3">
								<span><i class="fa fa-gift"></i> Quà tặng kèm</span>
								<ul>
									<li>Giảm ngay 500.000 (áp dụng đặt và nhận hàng từ 9 - 15/3) (đã trừ vào giá)</li>
									<li>Trả Góp 0% Qua Công Ty Tài Chính Và Ngân Hàng</li>
								</ul>
							</div>
							<hr>
							<div class="product-info-promotion py-3">
								<h3>Khuyến mãi</h3>
								<ul>
									<li>Chuột không dây</li>
									<li>Balo Laptop</li>
									<li>Phiếu Mua Hàng Online 200.000đ</li>
								</ul>
							</div>
							<div class="product-info-form py-3 clearfix">
								<form method="POST" action="">
									<button class="buy-now">
										<span class="buy-title"><i class="fas fa-shopping-cart" aria-hidden="true"></i> Mua hàng</span>
										<span class="buy-desc">Giao hàng miễn phí tận nơi</span>
									</button>
									<button class="buy-payment">
										<span class="buy-title"><i class="fa fa-credit-card" aria-hidden="true"></i> Mua hàng trả góp</span>
										<span class="buy-desc">Lãi xuất 0%</span>
									</button>
								</form>
							</div>
							<div class="product-info-policy">
								<p><i class="fa fa-phone" aria-hidden="true"></i> Gọi <span>0336 709 778</span> để được tư vấn và mua hàng miễn phí</p>
								<ul class="mt-4">
									<li><a href="#">Xem thêm chính sách bảo hành và đổi trả sản phẩm</a></li>
									<li>Hệ thống trung tâm bảo hành <a href="#" class="highlight">(Xem chi tiết)</a></li>
									<li>Đổi trả sản phẩm trong vòng 30 ngày <a href="#" class="highlight">(Nếu do lỗi kỹ thuật)</a></li>
									<li>Miễn phí vận chuyển toàn quốc <a href="#" class="highlight">(Xem chi tiết)</a></li>
								</ul>
							</div>
						</div>						
					</div>					
				</div>
				<div class="col-xl-3 d-none d-xl-block">
					<div class="product-detail-policy card">
						<div class="product-policy-item media">
							<img src="{{ asset('images/po1.png') }}" class="product-policy-image">
							<div class="product-policy-body media-body">
								<h5>Giao hàng tận nơi</h5>
								<p>Miễn phí vận chuyển với đơn hàng trên 300.000đ</p>
							</div>
						</div>
						<div class="product-policy-item media">
							<img src="{{ asset('images/po2.png') }}" class="product-policy-image">
							<div class="product-policy-body media-body">
								<h5>Thanh toán khi nhận hàng</h5>
								<p>Bạn thoải mái nhận & kiểm tra hàng trước khi trả tiền</p>
							</div>
						</div>
						<div class="product-policy-item media">
							<img src="{{ asset('images/po3.png') }}" class="product-policy-image">
							<div class="product-policy-body media-body">
								<h5>Đổi trả trong vòng 7 ngày</h5>
								<p>Dễ dàng đổi trả sản phẩm trong vòng 7 ngày đầu tiên</p>
							</div>
						</div>
						<div class="product-policy-item media">
							<img src="{{ asset('images/po4.png') }}" class="product-policy-image">
							<div class="product-policy-body media-body">
								<h5>Bảo hành chính hãng</h5>
								<p>Bạn thoải mái nhận và kiểm tra hàng trước khi trả tiền</p>
							</div>
						</div>
						<div class="product-policy-item media">
							<img src="{{ asset('images/po5.png') }}" class="product-policy-image">
							<div class="product-policy-body media-body">
								<h5>Đặt hàng online</h5>
								<p>Gọi ngay (04) 5560 3426 để mua và đặt hàng nhanh chóng</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		@include('frontend.components.related-post')

		<div class="product-detail-content">
			<div class="row">
				<div class="col-lg-8 order-lg-1 order-2">
					<div class="product-title">
						<h4 class="text-uppercase font-weight-bold">Giới thiệu sản phẩm</h4>
					</div>
					<article class="product-article my-4">
						{!! $product->pro_content !!}
					</article>
					<div class="product-detail-rate pt-4 border-top">
						<div class="product-rate-title pb-4">
							<h3 class="m-0">3 đánh giá SamSung Galaxy S20+</h3>
						</div>
						<div class="row product-rate-box">
							<div class="col-md-6 col-lg-5 product-rate-num">
								<div class="rate-border-left">
									<div class="product-rate-info">
										<span class="product-rate-star">5</span>
										<div class="progress product-rate-progress">
											<div class="progress-bar bg-warning" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<span class="product-rate-count">3 đánh giá</span>
									</div>
									<div class="product-rate-info">
										<span class="product-rate-star">4</span>
										<div class="progress product-rate-progress">
											<div class="progress-bar bg-warning" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<span class="product-rate-count">3 đánh giá</span>
									</div>
									<div class="product-rate-info">
										<span class="product-rate-star">3</span>
										<div class="progress product-rate-progress">
											<div class="progress-bar bg-warning" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
										</div class="product-rate-star">
										<span class="product-rate-count">3 đánh giá</span>
									</div>
									<div class="product-rate-info">
										<span class="product-rate-star">2</span>
										<div class="progress product-rate-progress">
											<div class="progress-bar bg-warning" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<span class="product-rate-count">3 đánh giá</span>
									</div>
									<div class="product-rate-info">
										<span class="product-rate-star">1</span>
										<div class="progress product-rate-progress">
											<div class="progress-bar bg-warning" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<span class="product-rate-count">3 đánh giá</span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-7 product-rate-button">
								<div class="rate-border-right">
									<a href="#">Gửi đánh giá của bạn</a>
								</div>
							</div>
						</div>
						<div class="product-rate-review py-3">
							<div class="rate-item pb-3">
								<span class="rate-user font-weight-bold mr-2">Tran Cuong</span>
								<span class="rate-time">3 ngày trước</span>
								<div class="product-rate-comment mt-2">
									<div class="rate-star float-left mr-3">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
									<p>Hôm nay mình đến lấy s20+ nhìn chung màn hình tràn viền cực đã lướt màn hình rất nhanh với cụm camera chụp nét cầm vừa tay rất thích</p>
								</div>
							</div>
							<div class="rate-item pb-3">
								<span class="rate-user font-weight-bold mr-2">Tran Cuong</span>
								<span class="rate-time">3 ngày trước</span>
								<div class="product-rate-comment mt-2">
									<div class="rate-star float-left mr-3">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
									<p>Hôm nay mình đến lấy s20+ nhìn chung màn hình tràn viền cực đã lướt màn hình rất nhanh với cụm camera chụp nét cầm vừa tay rất thích</p>
								</div>
							</div>
							<div class="rate-item pb-3">
								<span class="rate-user font-weight-bold mr-2">Tran Cuong</span>
								<span class="rate-time">3 ngày trước</span>
								<div class="product-rate-comment mt-2">
									<div class="rate-star float-left mr-3">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
									<p>Hôm nay mình đến lấy s20+ nhìn chung màn hình tràn viền cực đã lướt màn hình rất nhanh với cụm camera chụp nét cầm vừa tay rất thích</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 order-1">
					<div class="product-title">
						<h4 class="text-uppercase font-weight-bold">Thông số kỹ thuật</h4>
					</div>
					@include('frontend.components.attribute-product')
				</div>
			</div>
		</div>		
	</div>
</div>