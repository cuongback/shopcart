<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
include 'route-admin.php';

Route::group(['namespace' => 'Auth', 'prefix' => 'account'], function() {
	Route::get('login', 'LoginController@getLogin')->name('auth.product.login');
	Route::post('login', 'LoginController@postLogin')->name('auth.product.login');

	Route::get('register', 'RegisterController@index')->name('auth.product.register');
});

Route::group(['prefix' => ''], function() {
	Route::get('/', 'HomeController@index');
	Route::get('/cart', 'CartController@index')->name('layouts.product.cart');
	Route::get('/{cateSlug}/{slug}', 'ProductDetailController@index')->name('layouts.product.detail');
});


