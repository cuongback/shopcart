<?php


Route::group(['prefix' => 'shop-admin', 'namespace' => 'Admin'], function() {
	Route::get('/', function() {
		return view('admin.index');
	});

	/**
	 * Danh mục sản phẩm
	 **/

	Route::group(['prefix' => 'category'], function() {
		/*
		_______________________________
		|
		|  ADMIN CATEGORY
		|______________________________
		*/

		// Show Category table
		Route::get('', 'AdminCategoryController@index')->name('admin.category.index');
		Route::get('/list', 'AdminCategoryController@getList')->name('admin.category.list');
		// Create Category
		Route::get('create', 'AdminCategoryController@create')->name('admin.category.create'); // show form
		Route::post('create', 'AdminCategoryController@store'); // handle event create

		//update Category
		Route::get('update/{id}', 'AdminCategoryController@edit')->name('admin.category.update'); // show form
		Route::post('update/{id}', 'AdminCategoryController@update'); // handle event update

		Route::get('active/{id}', 'AdminCategoryController@active')->name('admin.category.active');
		Route::post('order', 'AdminCategoryController@order')->name('admin.category.order');

		//delete Category
		Route::get('delete/{id}', 'AdminCategoryController@delete()')->name('admin.category.delete()'); // show form
	});

	/*
	 * Sản phẩm
	 */

	Route::group(['prefix' => 'product'], function() {
		/*
		_______________________________
		|
		|  ADMIN PRODUCTS
		|______________________________
		*/

		Route::get('', 'AdminProductController@index')->name('admin.product.index');
		Route::get('/list', 'AdminProductController@getList')->name('admin.product.list');

		// create product
		Route::get('create', 'AdminProductController@create')->name('admin.product.create');
		Route::post('create', 'AdminProductController@store');

		// create product attribute value
		Route::post('create/attribute-values/{id}', 'AdminProductController@storeProductAttribute')->name('admin.productAttribute.create');

		// create image gallery
		Route::post('create/media', 'AdminProductController@storeMedia')->name('admin.image.create');
		Route::post('create/media/{id}', 'AdminProductController@storeImage')->name('admin.image.store');
		Route::get('delete/media/{id}', 'AdminProductController@deleteImagesGallery');

		// update product
		Route::get('update/{id}', 'AdminProductController@edit')->name('admin.product.update');
		Route::post('update/{id}', 'AdminProductController@update');

		// delete image
		Route::get('delete/image/{id}', 'AdminProductController@deleteProductImage')->name('admin.product.image.delete');
	});

	/*
	 * Thuộc tính sản phẩm
	 */

	Route::group(['prefix' => 'attribute'], function() {
		
		/*
		_______________________________
		|
		|  ADMIN ATTRIBUTE GROUP
		|______________________________
		*/

		// Show attribute group table
		Route::get('group', 'AdminAttributeGroupController@index')->name('admin.attributeGroup.index');
		Route::get('group/list', 'AdminAttributeGroupController@getList')->name('admin.attributeGroup.list');
		
		// Create attribute group
		Route::get('group/create', 'AdminAttributeGroupController@create')->name('admin.attributeGroup.create');
		Route::post('group/create', 'AdminAttributeGroupController@store');

		// update attribute group
		Route::get('/group/update/{id}', 'AdminAttributeGroupController@edit')->name('admin.attributeGroup.update');
		Route::post('/group/update/{id}', 'AdminAttributeGroupController@update');
		Route::post('/group/order', 'AdminAttributeGroupController@order')->name('admin.attributeGroup.order');

		/*
		_______________________________
		|
		|  ADMIN ATTRIBUTES
		|______________________________
		*/
		// show attribute
		Route::get('', 'AdminAttributeController@index')->name('admin.attribute.index');
		Route::get('/list', 'AdminAttributeController@getList')->name('admin.attribute.list');
		Route::post('/values', 'AdminAttributeController@getValueByAttribute')->name('admin.attributeValue.list');

		// Create attributes
		Route::get('/create', 'AdminAttributeController@create')->name('admin.attribute.create');
		Route::post('/create', 'AdminAttributeController@store')->name('admin.attribute.store');

		// update attributes
		Route::get('/update/{id}', 'AdminAttributeController@edit')->name('admin.attribute.update');
		Route::post('/update/{id}', 'AdminAttributeController@update');

		/*
		_______________________________
		|
		|  ADMIN ATTRIBUTES VALUES
		|______________________________
		*/
		// Create attribute value
		Route::post('/values/create/{id}', 'AdminAttributeValueController@store')->name('admin.attributeValue.store');		
	});

	/*
	 * Slider
	 */
	Route::group(['prefix' => 'slider'], function() {
		/*
		_______________________________
		|
		|  ADMIN SLIDER
		|______________________________
		*/
		// show view slider
		Route::get('', 'AdminSliderController@index')->name('admin.slider.index');
		Route::get('/list', 'AdminSliderController@getList')->name('admin.slider.list');

		// Create slider
		Route::get('/create', 'AdminSliderController@create')->name('admin.slider.create');
		Route::post('/create', 'AdminSliderController@store');

		// update slider
		Route::get('/update/{id}', 'AdminSliderController@edit')->name('admin.slider.update');
		Route::post('/update/{id}', 'AdminSliderController@update');
		Route::get('active/{id}', 'AdminSliderController@active')->name('admin.slider.active');

		// delete image
		Route::get('delete/image/{id}', 'AdminSliderController@deleteSliderImage')->name('admin.slider.image.delete');
	});
});
