<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'sliders';
    protected $guarded = [];

    public static function getHomeSlider() {
    	return Slider::select('id', 'image', 'link', 'title')
    	             ->where('status', '=', 1)
    	             ->orderBy('order', 'asc')
    	             ->get();
    }
}
