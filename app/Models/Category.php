<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'c_name', 'c_parentId', 'c_order', 'c_slug', 'c_avatar', 'c_description', 'c_status'
    ];
    protected $guarded =[];
    

    public function attributeGroup() {
        return $this->hasMany('App\Models\AttributeGroup', 'att_category_id');
    }

    public function parent() {
    	return $this->belongsTo('App\Models\Category', 'c_parentId');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'c_parentId');
    }

    public function hasItem() {
        return $this->children->isNotEmpty();
    }

    public function getParentsNames() {
	    if($this->parent) {
	        return $this->parent->getParentsNames(). " <span>&#8594;</span> " . $this->c_name;
	    } else {
	        return $this->c_name;
	    }
	}

    public static function listCategories() {
    	return Category::select('id', 'c_name', 'c_parentId', 'c_order')
    				   ->orderBy('c_parentId', 'asc')
    				   ->orderBy('c_order', 'asc')
    				   ->get();
    }

    /*____________________________________________
     |
     | FRONTEND FUNCTION
     |____________________________________________ */
    public static function getCategoryByParent($parentId = 0) {
        return Category::select('id', 'c_name', 'c_avatar', 'c_description', 'c_slug')
                       ->orderBy('c_order', 'asc')
                       ->where(['c_parentId' => $parentId, 'c_status' => true])
                       ->get();
    }

    public function getCategoryWithLimit() {
        return Category::select('id', 'c_name')
                       ->orderBY('c_order', 'asc')
                       ->where(['c_parentId' => $this->id, 'c_status' => true])
                       ->offset(0)
                       ->limit(4)
                       ->get();
    }
}
