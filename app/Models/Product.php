<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [];
    protected $guarded = [];

    public function category() {
    	return $this->belongsTo('App\Models\Category', 'pro_category_id');
    }

    public function images() {
    	return $this->hasMany('App\Models\ProductImage', 'product_id')->select('id', 'image');
    }

    public function attributes() {
    	return $this->hasMany('App\Models\ProductAttribute', 'product_id');
    }

    public function getAttributeGroupAttribute() {
        return $this->getAttribute('attributes')->groupBy('nameAttributeGroup');
    }

    public static function getAllProductHotNewByCategory($categories = array()) {
        $ids = implode(',', $categories);
        return DB::select("
            SELECT p.id, p.pro_name, p.pro_slug, p.pro_price, p.pro_price_sale, p.pro_sale, p.pro_avatar, c.c_slug
            FROM products as p
            JOIN categories as c
            ON p.pro_category_id = c.id
            WHERE p.pro_category_id IN ($ids) AND p.pro_active = 1
            ORDER BY p.created_at DESC
            LIMIT 9
        ");
    }

    public static function getAllProductByCategory($category) {
        return DB::select("
            SELECT p.id, p.pro_name, p.pro_slug, p.pro_price, p.pro_price_sale, p.pro_sale, p.pro_avatar, c.c_slug
            FROM products as p
            JOIN categories as c
            ON p.pro_category_id = c.id
            WHERE p.pro_category_id = $category AND p.pro_active = 1
            ORDER BY p.created_at DESC
            LIMIT 9
        ");
    }
}
