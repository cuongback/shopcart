<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeValue extends Model
{
    protected $table = 'product_attribute_value';
    protected $fillable = ['product_attribute_id', 'attribute_value_id'];

    public function attributeValue() {
    	return $this->belongsTo('App\Models\AttributeValue', 'attribute_value_id');
    }

    public function getValueAttribute() {
    	return $this->attributeValue->atb_val_name;
    }
}
