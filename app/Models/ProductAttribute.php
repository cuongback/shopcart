<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attribute';
    protected $fillable = [];
    protected $guarded = [];

    public function attribute() {
    	return $this->belongsTo('App\Models\Attribute', 'attribute_id');
    }

    public function values() {
    	return $this->hasMany('App\Models\ProductAttributeValue', 'product_attribute_id');
    }

    public function getNameAttribute() {
    	return $this->attribute->atb_name;
    }

    public function getNameAttributeGroupAttribute() {
    	return $this->attribute->attribute_group->att_name;
    }
}
