<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attributes';
    protected $guarded = [];

    /**
     *
     *  relationship 1-n (1 Attribute Group - n Attribute)
     *  
     *
     */
    public function attribute_group() {
    	return $this->belongsTo('App\Models\AttributeGroup', 'atb_group_id');
    }

    public function values() {
        return $this->hasMany('App\Models\AttributeValue', 'atb_id');
    }

    public function getDeleteId($values = []) {
        return $this->values()
                    ->pluck('id')
                    ->diff($values);
    }
}
