<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AttributeGroup extends Model
{
    protected $table = 'attribute_group';
    protected $guarded = [];

    /**
     *
     *  relationship 1-n (1 Category - n AttributeGroup)
     *  
     *
     */
    public function category() {
    	return $this->belongsTo('App\Models\Category', 'att_category_id');
    }

    public function attributes() {
        return $this->hasMany('App\Models\Attribute', 'atb_group_id');
    }

    public static function getCateOfAttribute() {
        return DB::select("
                            SELECT attr.att_category_id, cate.c_name
                            FROM attribute_group as attr, categories as cate
                            WHERE attr.att_category_id = cate.id
                            GROUP BY attr.att_category_id, cate.c_name
                        ");
    }
}
