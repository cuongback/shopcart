<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
	public static function getAllCategoryByParent($idCate=0) {
		$arr = array();
		$cateParent = Category::find($idCate);
;
		$cateChild = Category::select('id')
		                     ->where('c_parentId', $cateParent->id)
		                     ->orderBy('c_order', 'asc')
		                     ->get();

		foreach($cateChild as $val) {
			$arr[] = $val->id;
		}

		array_push($arr, $cateParent->id);

		return $arr;
	}
}
