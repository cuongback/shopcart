<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AdminRequestAttributeGroup;
use App\Http\Controllers\Admin\AdminCategoryController;
use App\Models\AttributeGroup;
use Carbon\Carbon;
use DataTables;

class AdminAttributeGroupController extends AdminController
{
    public function index() {        
    	return view('admin.attribute.attribute-group.index');
    }

    public function getList() {
        $attributes = AttributeGroup::latest()
                                    ->select('id', 'att_name', 'att_category_id', 'att_order')
                                    ->get();
        return DataTables::of($attributes)
                           ->addIndexColumn()
                           ->editColumn('att_category_id', function($data) {
                                return
                                '<a class="badge badge-success" href="'.route('admin.category.update', $data->att_category_id).'">'.$data->category->c_name.'</a>';
                           })
                           ->addColumn('action', function($data){
                                return
                                '<a href="'.route('admin.attributeGroup.update', $data->id).'" class="btn btn-xs btn-info"><i class="fa fa-pencil"> Edit</i></a>
                                 <a href="#" class="btn btn-danger btn-xs ml-2"><i class="fa fa-trash"> Delete</i></a>';
                            })
                           ->rawColumns(['action', 'att_category_id'])
                           ->make(true);;
    }

    public function create() {
    	$selecCate = AdminCategoryController::getSelect(0);
    	return view('admin.attribute.attribute-group.create', ['dataCate' => $selecCate]);
    }

    public function edit($id) {
    	$attributeGroup = AttributeGroup::find($id);

    	if($attributeGroup === null) {
    		return redirect()->route('admin.attributeGroup.index');
    	}

    	$listCate = AdminCategoryController::getSelectItem($attributeGroup->att_category_id);

    	return view('admin.attribute.attribute-group.update', compact('attributeGroup', 'listCate'));
    }

    public function order(Request $request) {
        $attributes = $request->attrOrder;
        if(count($attributes) && $request->isMethod('post')) {
            foreach($attributes as $stt => $attrId) {
                $order = $stt + 1;

                AttributeGroup::find($attrId)
                              ->update(['att_order' => $order]);
            }

            return redirect()->back();
        }
    }

    public function update(AdminRequestAttributeGroup $request, $id) {
    	$attributeGroup = AttributeGroup::find($id);

    	if($request->isMethod('post') && $attributeGroup !== null) {
    		$attributeGroup->att_name        = trim($request->txt_attName);
            $attributeGroup->att_category_id = $request->txt_cateId;
            $attributeGroup->att_order       = $request->txt_attOrder;
            $attributeGroup->updated_at      = Carbon::now();

            $attributeGroup->save();

            return redirect()->route('admin.attributeGroup.index')->with('success', 'You have successfully updated');
    	}
    }

    public function store(AdminRequestAttributeGroup $request) {
    	$tmpAttribute = AttributeGroup::where('att_category_id', $request->txt_cateId)
    								  ->where('att_name', trim($request->txt_attName))
    								  ->first();

    	if($request->isMethod('post')) {
    		if($tmpAttribute === null) {
    			$attributeGroup = new AttributeGroup;

		    	$attributeGroup->att_name 		 = trim($request->txt_attName);
		    	$attributeGroup->att_category_id = $request->txt_cateId;
		    	$attributeGroup->att_order		 = $request->txt_attOrder;
		    	$attributeGroup->created_at		 = Carbon::now();

		    	$attributeGroup->save();

		    	return redirect()->route('admin.attributeGroup.index')->with('success', 'You have successfully created');
    		} else {
    			return redirect()->back();
    		}
    		
    	}    	
    }

    public static function getAttributeGroupByCate() {
        return AttributeGroup::getCateOfAttribute();
    }
    
    public static function getAllAttributeGroup() {
        return AttributeGroup::select('id', 'att_name', 'att_category_id')
                             ->orderBy('att_order', 'asc')
                             ->get();
    }
}
