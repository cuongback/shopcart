<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminRequestAttribute;
use App\Http\Controllers\Admin\AdminAttributeGroupController;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\AttributeValue;
use Carbon\Carbon;
use Illuminate\Support\Str;
use DataTables;

class AdminAttributeController extends AdminController
{
    public function index() {
    	return view('admin.attribute.attribute-specification.index');
    }

    public function getList() {
    	$attribute = Attribute::latest()
    	                      ->select('id', 'atb_name', 'atb_group_id')
    	                      ->get();
		return DataTables::of($attribute)
						 ->addIndexColumn()
						 ->editColumn('atb_fillable', function($data) {
						 	return 'No';
						 })
						 ->editColumn('atb_group_id', function($data) {
						 	return
						 		'<a class="badge badge-success" href="'.route('admin.category.update', $data->attribute_group->att_category_id).'">'.Category::find($data->attribute_group->att_category_id)->c_name.'</a> <span>&#8594;</span> '.$data->attribute_group->att_name;
						 		 
						 })
						 ->addColumn('action', function($data) {
						 	return
                                '<a href="'.route('admin.attribute.update', $data->id).'" class="btn btn-xs btn-info"><i class="fa fa-pencil"> Edit</i></a>
                                 <a href="#" class="btn btn-danger btn-xs ml-2"><i class="fa fa-trash"> Delete</i></a>';
						 })
						 ->rawColumns(['action', 'atb_group_id'])
                         ->make(true);;
    }

    public function create() {
    	$categories = AdminAttributeGroupController::getAttributeGroupByCate();
    	$attributeGroup = AdminAttributeGroupController::getAllAttributeGroup();

    	return view('admin.attribute.attribute-specification.create', compact('categories', 'attributeGroup'));
    }

    public function edit($id) {
        $categories = AdminAttributeGroupController::getAttributeGroupByCate();
        $attributeGroup = AdminAttributeGroupController::getAllAttributeGroup();
    	$attribute = Attribute::find($id);
        $attributeValue = $attribute->values;
    	if($attribute !== null) {
    		return view('admin.attribute.attribute-specification.update', compact('attribute', 'categories', 'attributeGroup', 'attributeValue'));
    	}
    	return view('admin.attribute.attribute-specification.index');
    }

    public function update(AdminRequestAttribute $request, $id) {
        $attribute = Attribute::find($id);
        if($attribute !== null) {
            $attribute->atb_name        = $request->txt_attributeName;
            $attribute->atb_group_id    = $request->txt_attributeGroup;
            $attribute->atb_slug        = Str::slug($request->txt_attributeName);
            $attribute->updated_at      = Carbon::now();

            $attribute->save();
            return redirect()->route('admin.attribute.index')->with('success', 'You have successfully updated');
        }
        return redirect()->back();
    }

    public function store(AdminRequestAttribute $request) {
    	if($request->isMethod('post')) {
    		$attribute = new Attribute;

    		$attribute->atb_name 		= $request->txt_attributeName;
    		$attribute->atb_group_id 	= $request->txt_attributeGroup;
    		$attribute->atb_slug		= Str::slug($request->txt_attributeName);
    		$attribute->created_at 		= Carbon::now();

    		$attribute->save();

    		return redirect()->route('admin.attribute.index')->with('success', 'You have successfully created');
    	} else {
    		redirect()->back();
    	}
    }

    public function getValueByAttribute(Request $request) {
        $attribute = $request->attribute;

        $attr = Attribute::find($request->attribute)->values;

        return response()->json($attr);
    }
}
