<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminCategoryController;
use App\Http\Requests\AdminRequestProduct;
use App\Http\Requests\AdminRequestAttributeProduct;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Category;
use App\Models\AttributeGroup;
use App\Models\ProductAttributeValue;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Image;
use DataTables;
use Storage;

class AdminProductController extends AdminController
{
    public function index() {
    	return view('admin.product.index');
    }

    public function imageView() {
        $data = [];
        return view('admin.product.image', compact('data'));
    }

    public function getList() {
        $product = Product::latest()
                          ->select('id', 'pro_name', 'pro_avatar', 'pro_category_id', 'pro_price', 'pro_active', 'created_at')
                          ->get();
        return DataTables::of($product)
                         ->addIndexColumn()
                         ->editColumn('pro_avatar', function($data) {
                            return '<img src="'.pare_url_file($data->pro_avatar, 'products', 'small').'" width="80px;" height="80px">';
                         })
                         ->editColumn('pro_category_id', function($data) {
                            return $data->category->c_name;
                         })
                         ->editColumn('pro_price', function($data) {
                            return number_format($data->pro_price, 0, ',', '.');
                         })
                         ->editColumn('pro_active', function($data) {
                            $hide = '<a href="#" class="badge badge-secondary">Hide</a>';
                            if($data->pro_active === 1)
                                $hide = '<a href="#" class="badge badge-success">Show</a>';
                            return $hide;

                         })
                         ->addColumn('action', function($data){
                            return
                            '<a href="'.route('admin.product.update', $data->id).'" class="btn btn-xs btn-info"><i class="fa fa-pencil"> Edit</i></a>
                             <a href="#" class="btn btn-danger btn-xs ml-2"><i class="fa fa-trash"> Delete</i></a>';
                         })
                         ->rawColumns(['pro_avatar', 'action', 'pro_active'])
                         ->make(true);
    }

    public function edit($id) {
        $product = Product::find($id);        

        if($product !== null) {
            $attributeGroup = AttributeGroup::where(['att_category_id' => $product->pro_category_id])
                                        ->select('id', 'att_name')
                                        ->orderBy('att_order', 'asc')
                                        ->get();
            $listCate = AdminCategoryController::getSelectItem($product->pro_category_id);
            return view('admin.product.update', compact('product', 'listCate', 'attributeGroup'));
        }
        return redirect()->route('admin.product.index');
    }

    public function update(AdminRequestProduct $request, $id) {
        $product = Product::find($id);
        $data = $request->all();

        if($product !== null && $request->isMethod('post')) {
            $product->pro_name          = $data['txt_proName'];
            $product->pro_slug          = Str::slug($data['txt_proName']);
            $product->pro_price         = $data['txt_proPrice'];
            $product->pro_price_entry   = $data['txt_proPriceEntry'];
            $product->pro_price_sale    = $data['txt_proPriceSale'];
            $product->pro_sale          = $data['txt_proSale'];
            $product->pro_category_id   = $data['txt_cateId'];
            $product->pro_active        = number_format($data['txt_status']);
            $product->updated_at        = Carbon::now();
            if(!empty($data['txt_proDescription'])) {
                $product->pro_description = $data['txt_proDescription'];
            }
            else {
                $product->pro_description = '';
            }

            if(!empty($data['txt_proContent'])) {
                $product->pro_content = $data['txt_proContent'];
            }
            else {
                $product->pro_content = '';
            }

            $file = $request->file('txt_image');
            if(!empty($file)) {
                $image = upload_image('txt_image', 'products');
                if($image['code'] == 1) {
                    $product->pro_avatar = $image['name'];
                    $large_image_path = $image['path_folder'].'large/'.$image['name'];
                    $medium_image_path = $image['path_folder'].'medium/'.$image['name'];
                    $small_image_path = $image['path_folder'].'small/'.$image['name'];

                    // resize image
                    Image::make($file)->save($large_image_path);
                    Image::make($file)->resize(600,600)->save($medium_image_path);
                    Image::make($file)->resize(300,300)->save($small_image_path);
                }                
            }

            $product->save();
            return redirect()->route('admin.product.index')->with('success', 'You have successfully created a product!');
        }
    }

    public function create() {
    	$selectCate = AdminCategoryController::getSelect(0);
    	return view('admin.product.create', ['optionCate' => $selectCate]);
    }

    public function store(AdminRequestProduct $request) {
    	if($request->isMethod('post')) {
    		$data = $request->all();
            
    		if(empty($data['txt_cateId'])) {
    			return redirect()->route('admin.product.index')->with('error', 'Category is missing!');
    		}

    		$product = new Product;
    		$product->pro_name 			= $data['txt_proName'];
    		$product->pro_slug 			= Str::slug($data['txt_proName']);
    		$product->pro_price 		= $data['txt_proPrice'];
    		$product->pro_price_entry 	= $data['txt_proPriceEntry'];
    		$product->pro_price_sale 	= $data['txt_proPriceSale'];
    		$product->pro_sale 			= $data['txt_proSale'];
    		$product->pro_category_id 	= $data['txt_cateId'];
            $product->pro_active        = number_format($data['txt_status']);
            $product->created_at        = Carbon::now();
    		if(!empty($data['txt_proDescription'])) {
    			$product->pro_description = $data['txt_proDescription'];
    		}
    		else {
    			$product->pro_description = '';
    		}

    		if(!empty($data['txt_proContent'])) {
    			$product->pro_content = $data['txt_proContent'];
    		}
    		else {
    			$product->pro_content = '';
    		}

    		$file = $request->file('txt_image');
    		if(!empty($file)) {
    			$image = upload_image('txt_image', 'products');
    			if($image['code'] == 1) {
    				$product->pro_avatar = $image['name'];
                    $large_image_path = $image['path_folder'].'large/'.$image['name'];
                    $medium_image_path = $image['path_folder'].'medium/'.$image['name'];
                    $small_image_path = $image['path_folder'].'small/'.$image['name'];

                    // resize image
                    Image::make($file)->save($large_image_path);
                    Image::make($file)->resize(600,600)->save($medium_image_path);
                    Image::make($file)->resize(300,300)->save($small_image_path);
    			}                
    		}

            $product->save();
            return redirect()->route('admin.product.index')->with('success', 'You have successfully created a product!');
    	}
    }

    public function deleteProductImage($id = null) {
        $product = Product::findOrFail($id);
        $large_image_path = public_path().'/images/products/'.date('Y/').'/large/'.$product->pro_avatar;
        $medium_image_path = public_path().'/images/products/'.date('Y/').'/medium/'.$product->pro_avatar;
        $small_image_path = public_path().'/images/products/'.date('Y/').'/small/'.$product->pro_avatar;
        if(file_exists($large_image_path)) {
            unlink($large_image_path);
        }
        if(file_exists($medium_image_path)) {
            unlink($medium_image_path);
        }
        if(file_exists($small_image_path)) {
            unlink($small_image_path);
        }
        $product->update(['pro_avatar' => '']);
        return redirect()->back()->with('success', 'Product image has been deleted successfully!');
    }

    public function deleteImagesGallery($id = null) {
        $productImage = ProductImage::findOrFail($id);
        $large_image_path = public_path().'/images/products/'.date('Y/').'/large/'.$productImage->image;
        $medium_image_path = public_path().'/images/products/'.date('Y/').'/medium/'.$productImage->image;
        $small_image_path = public_path().'/images/products/'.date('Y/').'/small/'.$productImage->image;
        if(file_exists($large_image_path)) {
            unlink($large_image_path);
        }
        if(file_exists($medium_image_path)) {
            unlink($medium_image_path);
        }
        if(file_exists($small_image_path)) {
            unlink($small_image_path);
        }
        $productImage->delete();
        return redirect()->back()->with('success', 'Product image has been deleted successfully!');
    }

    public function storeMedia(Request $request)
    {
        $path = storage_path('tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

    public function storeImage(Request $request, $id) {
        $product = Product::findOrFail($id);
        $pathFolder = 'images/products/'.date('Y/');

        if(count($request->gallery)) {
            foreach($request->gallery as $file) {
                $large_image_path = $pathFolder.'large/'.$file;
                $medium_image_path = $pathFolder.'medium/'.$file;
                $small_image_path = $pathFolder.'small/'.$file;

                Image::make(storage_path('tmp/uploads/' . $file))->save($large_image_path);
                Image::make(storage_path('tmp/uploads/' . $file))->resize(600,600)->save($medium_image_path);
                Image::make(storage_path('tmp/uploads/' . $file))->resize(300,300)->save($small_image_path);
                
                $productImage = new ProductImage;

                $productImage->image = $file;
                $productImage->product_id = $product->id;
                $productImage->created_at = Carbon::now();

                $productImage->save();
            }

            return redirect()->back()->with('success', 'Images has been added successfully!');
        }
        
    }

    public function storeProductAttribute(AdminRequestAttributeProduct $request, $id) {
        $product = Product::find($id);
        if($product !== null && $request->isMethod('post')) {

            $product->attributes()->delete();

            $data = $request->all();
            $productAttributeValues = [];

            foreach($data['attributes'] as $attr) {
                $productAttribute = $product->attributes()->create([
                    'attribute_id' => $attr['attribute_id'],
                    'product_id'   => $product->id
                ]);

                foreach($attr['values'] as $val) {
                    $productAttributeValues[] = [
                        'product_attribute_id' => $productAttribute->id,
                        'attribute_value_id' => $val
                    ];
                }
            }
            ProductAttributeValue::insert($productAttributeValues);
            return redirect()->back()->with('success', 'Attribute for product updated successfully');
        }
    }
}
