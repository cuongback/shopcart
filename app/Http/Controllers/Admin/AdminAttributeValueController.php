<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AttributeValue;
use App\Models\AttriBute;
use Illuminate\Support\Arr;

class AdminAttributeValueController extends AdminController
{
    public function store(Request $request, $id) {
    	$attribute = AttriBute::find($request->id);
    	if(!empty($attribute)) {    		
    		$getValueId = $attribute->getDeleteId($request->txtAttributeValueId);

    		if($getValueId->isNotEmpty()) {
    			$attribute->values()->whereIn('id', $getValueId)->delete();
    		}
            foreach($request->txtAttribute as $val) {
                $attribute->values()->updateOrCreate(
                    ['id' => $val['id']],
                    ['atb_val_name' => $val['value'], 'atb_id' => $id]
                );
            }
    		return redirect()->route('admin.attribute.index')->with('success', 'You have successfully updated');
    	}              	
    }
}
