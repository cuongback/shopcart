<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminRequestCategory;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\Category;
use RealRashid\SweetAlert\Facades\Alert;
use DataTables;
use Image;

class AdminCategoryController extends AdminController
{
    // show list category page
    public function index() {
    	return view('admin.category.index');
    }

    public function getList() {
        $categories = Category::latest()->get();
        return DataTables::of($categories)
                ->addIndexColumn()
                ->editColumn('c_status', function($data) {
                    $hide = '<a href="'.route('admin.category.active', $data->id).'" class="badge badge-secondary">Hide</a>';
                    if($data->c_status === 1)
                        $hide = '<a href="'.route('admin.category.active', $data->id).'" class="badge badge-success">Show</a>';
                    return $hide;

                })
                ->editColumn('c_parentId', function($data) {
                    if($data->getParentsNames()) {
                        if($data->c_parentId !== 0)
                            return $data->getParentsNames();
                        else
                            return '';
                    }                    
                })
                ->addColumn('action', function($data){
                    return
                    '<a href="'.route('admin.category.update', $data->id).'" class="btn btn-xs btn-info"><i class="fa fa-pencil"> Edit</i></a>
                     <a href="#" class="btn btn-danger btn-xs ml-2"><i class="fa fa-trash"> Delete</i></a>';
                })
                ->rawColumns(['action', 'c_status', 'c_parentId'])
                ->make(true);
    }

    // show form create of category page
    public function create() {
        $listCategories = $this->getSelect(0);

    	return view('admin.category.create', ['selectCategory' => $listCategories]);
    }

    // show form update of category page
    public function edit($id) {
        $category = Category::find($id);
        $listCateChild = [];

        if($category == null) {
            return redirect()->route('admin.category.index')->with('error', 'Whoops, category does not exists');
        }
        else {
            $tmp = Category::where('c_parentId', $id)->count();

            if($tmp > 0) {
                $listCateChild = Category::where('c_parentId', $id)
                                           ->select('id', 'c_name')
                                           ->orderBy('c_order', 'asc')
                                           ->get();
            }
            $listOption = $this->getSelect($id);
            $attributes = Category::find($id)->attributeGroup()->orderBy('att_order', 'asc')->get();            

            return view('admin.category.update', [
                'category'      => $category,
                'listOption'    => $listOption,
                'listCateChild' => $listCateChild,
                'listAttribute' => $attributes
            ]);
        }
    }

    // order child category of parent category
    public function order(Request $request) {        
        if(count($request->order)) {
            foreach($request->order as $stt => $idCateChild) {
                $order = $stt + 1;

                Category::find($idCateChild)
                        ->update(['c_order' => $order]);
            }
            return redirect()->back();
        }
        return redirect()->route('admin.category.index');
    }

    // update status (show / hide) category
    public function active($id) {
    	$category = Category::find($id);

    	$category->c_status = !$category->c_status;
    	$category->save();

    	return redirect()->back();
    }

    // method insert category to database
    public function store(AdminRequestCategory $request) {
    	
        if($request->c_parentId > 0 && Category::find($request->c_parentId) != null || $request->c_parentId == 0) {

            $category = new Category;

            $category->c_name       = $request->c_name;
            $category->c_slug       = Str::slug($request->c_name);
            $category->c_parentId   = $request->c_parentId;
            $category->c_description = $request->c_description;
            $category->created_at   = Carbon::now();

            $file = $request->file('c_avatar');
            if(!empty($file)) {
                $avatar = upload_image('c_avatar', 'category');

                if($avatar['code'] === 1) {
                    $category->c_avatar = $avatar['name'];
                    $pathImage = $avatar['path_folder'].$avatar['name'];
                    Image::make($file)->resize(168,168)->save($pathImage);
                }
            }

            if(Category::where('c_parentId', $request->c_parentId)->count() == 0){
                $category->c_order = 1;
            } else {
                $category->c_order = Category::where('c_parentId', $request->c_parentId)
                                           ->max('c_order') + 1;
            }

            $category->save();

            return redirect()->route('admin.category.index')->with('success', 'You have successfully created a Category!');;
        }
        else {
            return redirect()->route('admin.category.index')->with('error', "Whoops, something went wrong");
        }
        
    }

    // method update category to dabase
    public function update(AdminRequestCategory $request, $id) {
        $categoryChild = Category::find($request->c_parentId); // check category have exists in parent category
        $tmp = Category::where('c_parentId', $id)->get();

        foreach($tmp as $val) {
            if($val->id == $request->c_parentId) {
                return redirect()->route('admin.category.index')->with('error', "Whoops, something went wrong");
            }
        }

        if(($request->c_parentId > 0 && $request->btn_edit == 'edit' && $categoryChild != null) || $request->c_parentId == 0) {
            $ckCategory = Category::find($id);
            
            if($ckCategory != null) {
                $ckCategory->c_name       = $request->c_name;
                $ckCategory->c_slug       = Str::slug($request->c_name);
                $ckCategory->c_description = $request->c_description;
                $ckCategory->updated_at   = Carbon::now();
                $file = $request->file('c_avatar');
                if(!empty($file)) {
                    $avatar = upload_image('c_avatar', 'category');

                    if($avatar['code'] === 1) {
                        $ckCategory->c_avatar = $avatar['name'];
                        $pathImage = $avatar['path_folder'].$avatar['name'];

                        Image::make($file)->resize(168,168)->save($pathImage);
                    }
                }

                if($ckCategory->c_parentId != $request->c_parentId) {
                    $cateParent = Category::where('c_parentId', $request->c_parentId);
                    if($cateParent->count() == 0) {
                        $ckCategory->c_order = 1;
                    }
                    else {
                        $ckCategory->c_order = $cateParent->max('c_order') + 1;
                    }

                    $ckCategory->c_parentId = $request->c_parentId;
                }
                
                $ckCategory->save();
                return redirect()->route('admin.category.index')->with('success', 'You have successfully update category!');;
            }
            return 'Error';
        }
        return redirect()->route('admin.category.index')->with('error', 'Updated not successfully');
    }

    public static function getSelect($id) {
        return AdminCategoryController::selectCategories($id, Category::listCategories());
    }

    public static function getSelectItem($id) {
        return AdminCategoryController::selectCategoriesItem($id, Category::listCategories());
    }

    // de quy hien thi chuyen muc con
    public static function selectCategoriesItem($id, $categories, $parentId = 0, $char = '|---', $tableStr = '')
    {
        foreach($categories as $key => $item) {
            if($item->c_parentId == $parentId) {
                //echo $item->id;
                if($item->id == $id) {                    
                    $tableStr.= "<option class='".($parentId == 0 ? 'high': '')."' value='$item->id' selected>";
                } else {
                    $tableStr.= "<option class='".($parentId == 0 ? 'high': '')."' value='$item->id'>";
                }

                $tableStr.= $char.$item->c_name;
                $tableStr.= "</option>";
                // Xóa chuyên mục đã lặp
                unset($categories[$key]);
                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                $tableStr = AdminCategoryController::selectCategoriesItem($id, $categories, $item->id, $char.'|------', $tableStr);
            }           
        }
        
        return $tableStr;
    }

    // de quy hien thi chuyen muc con theo chuyen muc cha
    public static function selectCategories($id, $categories, $parentId = 0, $char = '|---', $tableStr = '') {
        $selectId = 0;

        foreach($categories as $cateItem) {
            if($id == $cateItem->id){
                $selectId = $cateItem->c_parentId;
            }
        }

        foreach($categories as $key => $item) {
            if($item->c_parentId == $parentId) {
                //echo $item->id;
                if($item->id == $selectId) {                    
                    $tableStr.= "<option class='".($parentId == 0 ? 'high': '')."' value='$item->id' selected>";
                } else {
                    $tableStr.= "<option class='".($parentId == 0 ? 'high': '')."' value='$item->id'>";
                }

                $tableStr.= $char.$item->c_name;
                $tableStr.= "</option>";
                // Xóa chuyên mục đã lặp
                unset($categories[$key]);
                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                $tableStr = AdminCategoryController::selectCategories($id, $categories, $item->id, $char.'|------', $tableStr);
            }           
        }
        

        return $tableStr;
    }
}
