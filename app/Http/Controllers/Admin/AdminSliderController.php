<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AdminRequestSlider;
use App\Models\Slider;
use Carbon\Carbon;
use Image;
use DataTables;

class AdminSliderController extends Controller
{
    public function index() {
    	return view('admin.slider.index');
    }

    public function getList() {
    	$slide = Slider::latest()->get();
    	return DataTables::of($slide)
    	                 ->addIndexColumn()
    	                 ->editColumn('image', function($data) {
    	                 	return '<img class="img-fluid" width="200px;" src="'.pare_url_file($data->image, 'slider').'" alt="">';
    	                 })
    	                 ->editColumn('status', function($data) {
		                    $hide = '<a href="'.route('admin.slider.active', $data->id).'" class="badge badge-secondary">Hide</a>';
		                    if($data->status === 1)
		                        $hide = '<a href="'.route('admin.slider.active', $data->id).'" class="badge badge-success">Show</a>';
		                    return $hide;

			             })
    	                 ->addColumn('action', function($data){
		                    return
		                    '<a href="'.route('admin.slider.update', $data->id).'" class="btn btn-xs btn-info"><i class="fa fa-pencil"> Edit</i></a>
		                     <a href="#" class="btn btn-danger btn-xs ml-2"><i class="fa fa-trash"> Delete</i></a>';
		                })
    	                ->rawColumns(['action', 'image', 'status'])
                        ->make(true);
    }

    public function create() {
    	return view('admin.slider.create');
    }

    public function edit($id) {
    	$slide = Slider::findOrFail($id);

    	return view('admin.slider.update', compact('slide'));
    }

    public function store(AdminRequestSlider $request) {
    	$data = $request->all();
    	if($request->isMethod('post')) {
    		$slide = new Slider;

    		$slide->title      = $data['txt_title'];
    		$slide->link       = $data['txt_link'];
    		$slide->status     = $data['txt_status'];
    		$slide->order      = $data['txt_order'];
    		$slide->created_at = Carbon::now();

    		$file = $request->file('txt_image');
    		if(!empty($file)) {
    			$image = upload_image('txt_image', 'slider');
    			if($image['code'] === 1) {    				
    				$pathImage = $image['path_folder'].$image['name'];
                    Image::make($file)->resize(800,390)->save($pathImage);

                    $slide->image = $image['name'];
    			}
    		}

    		$slide->save();
    		return redirect()->route('admin.slider.index')->with('success', 'Slider has been added successfully!');
    	}
    	return redirect()->back();
    }

    public function update(AdminRequestSlider $request, $id) {
    	$slide = Slider::findOrFail($id);

    	if($request->isMethod('post')) {
    		$data = $request->all();

    		$slide->title      = $data['txt_title'];
    		$slide->link       = $data['txt_link'];
    		$slide->status     = $data['txt_status'];
    		$slide->order      = $data['txt_order'];
    		$slide->updated_at = Carbon::now();

    		$file = $request->file('txt_image');
    		if(!empty($file)) {
    			$image = upload_image('txt_image', 'slider');
    			if($image['code'] === 1) {    				
    				$pathImage = $image['path_folder'].$image['name'];
                    Image::make($file)->resize(800,390)->save($pathImage);

                    $slide->image = $image['name'];
    			}
    		}

    		$slide->save();
    		return redirect()->route('admin.slider.index')->with('success', 'Slider has been updated successfully!');
    	}
    }

    public function active($id) {
    	$slide = Slider::find($id);

    	$slide->status = !$slide->status;

    	$slide->save();
    	return redirect()->back();
    }

    public function deleteSliderImage($id = null) {
    	$slide = Slider::findOrFail($id);

    	$imgPath = public_path().'/images/slider/'.date('Y/').$slide->image;

    	if(file_exists($imgPath)) {
    		unlink($imgPath);
    	}

    	$slide->update(['image' => '']);
    	return redirect()->back()->with('success', 'Slider image has been deleted successfully!');
    }
}
