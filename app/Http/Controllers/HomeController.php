<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Slider;
use App\Http\Controllers\CategoryController;

class HomeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index() {
    	$categories = Category::getCategoryByParent(0);
    	$sliders = Slider::getHomeSlider();
    	$productCate = [];
    	foreach($categories as $value) {
    		$listCate = CategoryController::getAllCategoryByParent($value->id);
    		$listCateLimit = array_slice($listCate, 0, 4);
    		
    		foreach($listCateLimit as $val) {
    			$productCate[$val] = Product::getAllProductByCategory($val);
    		}
    		$productHotNew[$value->id] = Product::getAllProductHotNewByCategory($listCate);

    	}
    	
    	return view('frontend.pages.home', compact('productHotNew', 'productCate', 'sliders'));
    }
}
