<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class ProductDetailController extends Controller
{
    function index($cateSlug = null, $slug = null) {
    	$category = Category::where('c_slug', $cateSlug)
			    			->select('id')
			    			->firstOrFail();
    	
    	$product = Product::where(['pro_slug' => $slug, 'pro_category_id' => $category->id])
		                  ->select('products.id',
		                           'products.pro_name',
		                            'products.pro_slug', 
		                            'products.pro_price',
		                            'products.pro_sale',
		                            'products.pro_price_sale',
		                            'products.pro_avatar',
		                            'products.pro_description',
		                            'products.pro_content',
		                            'c.c_slug')
		                  ->join('categories as c', 'products.pro_category_id', '=', 'c.id')
		                  ->firstOrFail();

		$relatedPro = Product::where('pro_slug', '!=', $slug)
		                     ->where('pro_category_id', $category->id)
		                     ->select('products.id',
			                           'products.pro_name',
			                            'products.pro_slug', 
			                            'products.pro_price',
			                            'products.pro_sale',
			                            'products.pro_price_sale',
			                            'products.pro_avatar',
			                            'products.pro_description',
			                            'c.c_slug')
		                     ->join('categories as c', 'products.pro_category_id', '=', 'c.id')
		                     ->orderBy('products.created_at', 'desc')
		                     ->offset(0)
		                     ->limit(6)
		                     ->get();
		return view('frontend.pages.product-detail', compact('product', 'relatedPro'));
    }
}
