<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequestAttribute extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txt_attributeName' => 'required',
            'txt_attributeGroup' => 'required'
        ];
    }

    public function messages() {
        return [
            'txt_attributeName.required' => 'Dữ liệu không được để trống',
            'txt_attributeGroup.required' => 'Dữ liệu không được để trống'
        ];
    }
}
