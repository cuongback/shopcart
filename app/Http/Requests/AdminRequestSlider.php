<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequestSlider extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txt_image' => 'required',
            'txt_link' => 'required',
            'txt_order' => 'required'
        ];
    }

    public function messages() {
        return [
            'txt_image.required' => 'Ảnh không được để trống',
            'txt_link.required' => 'Link không được để trống'
        ];
    }
}
