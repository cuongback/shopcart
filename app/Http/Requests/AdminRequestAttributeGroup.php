<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AdminRequestAttributeGroup extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'txt_attName' => 'required|unique:attribute_group,att_name,'.$this->id.',id,att_category_id,'.$request->input('txt_cateId'),
            'txt_cateId' => 'required',
            'txt_attOrder' => 'required|numeric'
        ];
    }

    public function messages() {
        return [
            'txt_attName.unique'      => 'Dữ liệu đã tồn tại',
            'txt_attName.required'      => 'Dữ liệu không được để trống',
            'txt_cateId.required'       => 'Dữ liệu không được để trống',
            'txt_attOrder.required'     => 'Dữ liệu không được để trống',
            'txt_attOrder.numeric'      => 'Dữ liệu là dạng số',
        ];
    }
}
