<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequestProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txt_proName' => 'required|unique:products,pro_name,'.$this->id,
            'txt_proPriceEntry' => 'required',
            'txt_proPrice' => 'required',
            'txt_proSale' => 'required',
            'txt_proPriceSale' => 'required',
            'txt_cateId' => 'required'
        ];
    }
    public function messages() {
        return [
            'txt_proName.required' => 'Dữ liệu không được để trống',
            'txt_proPrice.required' => 'Dữ liệu không được để trống',
            'txt_proSale.required' => 'Dữ liệu không được để trống',
            'txt_proPriceEntry.required' => 'Dữ liệu không được để trống',
            'txt_cateId.required' => 'Dữ liệu không được để trống',
            'txt_proName.unique'      =>   'Tên sản phẩm đã tồn tại',
        ];
    }
}
