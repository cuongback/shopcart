<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAttributeValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attribute_value', function (Blueprint $table) {
            $table->integer('product_attribute_id')->unsigned();
            $table->integer('attribute_value_id')->unsigned();
            
            $table->primary(['product_attribute_id', 'attribute_value_id'], 'product_attribute_id_attribute_value_id_primary');

            $table->foreign('product_attribute_id')
                  ->references('id')
                  ->on('product_attribute')
                  ->onDelete('cascade');

            $table->foreign('attribute_value_id')
                  ->references('id')
                  ->on('attribute_values')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attribute_value');
    }
}
